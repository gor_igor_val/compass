﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Compass.Startup))]
namespace Compass
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
