//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Compass.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class APP_COMPANY
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public APP_COMPANY()
        {
            this.APP_COMPANY_QUALITY_PARAMS = new HashSet<APP_COMPANY_QUALITY_PARAMS>();
            this.APP_ServiceObjects = new HashSet<APP_ServiceObjects>();
            this.AspNetUsers = new HashSet<AspNetUsers>();
        }
    
        public int company_id { get; set; }
        public string name { get; set; }
        public string adress { get; set; }
        public string contact_person { get; set; }
        public string number { get; set; }
        public string email { get; set; }
        public string link_to_file_agreement { get; set; }
        public bool need_validation { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<APP_COMPANY_QUALITY_PARAMS> APP_COMPANY_QUALITY_PARAMS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<APP_ServiceObjects> APP_ServiceObjects { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AspNetUsers> AspNetUsers { get; set; }
    }
}
