﻿using Compass.EntityModels;
using System.Linq;

namespace Compass.Utils
{
    public class Features
    {
        private static CompassEntities db = new CompassEntities();

        public static string GetActiveUserName(string guid)
        {
            string s = string.Empty;
            if (!string.IsNullOrWhiteSpace(guid))
            {
                var user = db.AspNetUsers.First(item => item.Id == guid);
                if (user != null)
                {
                    s = user.FirstName + " " + user.LastName;
                }
            }
            return s;
        }

    }
}