﻿using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Web.Configuration;

namespace Compass.Utils
{
    class EmailClient
    {
        //По умолчанию работа с сервисом Gmail, основные настройки SMTP в Web.config
        private readonly SmtpClient _smtpClient;
        private readonly string _mailAddress;
        private readonly SmtpSection _config;

        public EmailClient()
        {
            _config = (SmtpSection)WebConfigurationManager.GetSection("system.net/mailSettings/smtp");
            _mailAddress = _config.From;
            _smtpClient = new SmtpClient();
        }

        /// <summary>
        /// Метод для отправки електронного сообщения, адрес отправителя указан в веб конфиге
        /// </summary>
        /// <param name="adressTo">Адрес получателя</param>
        /// <param name="subject">Тема сообщения</param>
        /// <param name="text">Тело сообщения</param>
        /// <param name="isHtml">Воспринимать тело сообщения как код HTML, по умолчанию false</param>
        public void SendMessage(string adressTo, string subject, string text, bool isHtml = false)
        {
            MailAddress from = new MailAddress(_mailAddress, "KompassAdmin");
            MailAddress to = new MailAddress(adressTo);
            MailMessage message = new MailMessage(from, to)
            {
                Subject = subject,
                Body = text,
                IsBodyHtml = isHtml
            };
            _smtpClient.Send(message);
        }
    }
}
