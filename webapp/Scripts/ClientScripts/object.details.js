﻿function initMap() {
    var map = new google.maps.Map(document.getElementById("map"), {
        zoom: 7,
        center: { lat: 49.272, lng: 31.685 }
    });
    var geocoder = new google.maps.Geocoder();

    addGeocodeButton(geocoder, map);

    var address = getAddress();
    if (address !== null) {
        geocodeAddress(geocoder, address, function (result) {
            placeMarker(map, result.location, result.address);
        });
    }
}