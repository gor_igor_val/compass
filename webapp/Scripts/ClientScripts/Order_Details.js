﻿$(document).ready(function () {
    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        pasteZone: null,
        autoUpload: false,
        disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
        maxFileSize: 5000000,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|doc?x)$/i,
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},                
    });

    $('#fileupload').addClass('fileupload-processing');
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: '/Order/GetFileList',
        dataType: "json",
        data: JSON.stringify({ path: $('#DocumentLink').val() }),
        success: function (data) {
            $('#fileupload').fileupload('option', 'done').call($('#fileupload'), $.Event('done'), { result: { files: data.files } })
            $('#fileupload').removeClass('fileupload-processing');
        }
    });

    var hdr = {
        left: 'title',
        center: 'month,agendaWeek,agendaDay',
        right: 'prev,today,next'
    };

    /* initialize the calendar
     -----------------------------------------------------------------*/
    $('#calendar').fullCalendar({

        header: hdr,
        buttonText: {
            prev: '<i class="fa fa-chevron-left"></i>',
            next: '<i class="fa fa-chevron-right"></i>'
        },

        editable: false,
        droppable: false, // this allows things to be dropped onto the calendar !!!
        timezone: 'local',

        windowResize: function (event, ui) {
            $('#calendar').fullCalendar('render');
        }
    });

    /* hide default buttons */
    $('.fc-header-right, .fc-header-center').hide();

    $('#calendar-buttons #btn-prev').click(function () {
        $('.fc-button-prev').click();
        return false;
    });

    $('#calendar-buttons #btn-next').click(function () {
        $('.fc-button-next').click();
        return false;
    });

    $('#calendar-buttons #btn-today').click(function () {
        $('.fc-button-today').click();
        return false;
    });

    $('#mt').click(function () {
        $('#calendar').fullCalendar('changeView', 'month');
    });

    $('#ag').click(function () {
        $('#calendar').fullCalendar('changeView', 'agendaWeek');
    });

    $('#td').click(function () {
        $('#calendar').fullCalendar('changeView', 'agendaDay');
    });

    for (var elem = 0; elem < events.length; elem++) {
        $('#calendar').fullCalendar('renderEvent', events[elem], true);
    }

})