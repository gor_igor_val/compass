﻿var tableOption = {
    "language": {
        "url": resource.tableLang
    },
    'columnDefs': [
        {
            "targets": "no-sort",
            "bSortable": false
        }
    ]
};

$(document).ready(function () {

    var table = $('#companyTable').dataTable(tableOption);

    $('#companyTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $('#DeleteCompany').hide();
            $('#EquipmentCompany').hide();
            $('#OrderCompany').hide();
            $('#EditCompany').hide();
            $('#UsersCompany').hide();
            $(this).removeClass('selected');
        }
        else {
            var companyId = $(this).attr('id');
            var companyName = $(this).attr('company-name');
            $('#DeleteCompany').attr('data-company-id', companyId).attr('data-company-name', companyName);
            $('#EditCompany').attr('action', "/Company/Edit/" + companyId);
            $('#DeleteCompany').show();
            $('#EditCompany').show();
            $('#UsersCompany').show();
            $('#UsersCompany').attr('action', "/users/Index/" + companyId);
            $('#EquipmentCompany').show();
            $('#EquipmentCompany').attr('action', "/Equipment/Index/" + companyId);
            $('#OrderCompany').show();
            $('#OrderCompany').attr('action', "/Order/Index/" + companyId);
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });


    // $('#companyTable').dataTable(tableOption);

    $(".viewDialog").on("click", function (e) {
        e.preventDefault();

        var name = $(this).attr('data-company-name');
        var id = $(this).attr('data-company-id');

        $.SmartMessageBox({
            title: "<i class='fa fa-warning txt-color-orangeDark'></i><span class='txt-color-orangeDark'><strong> " + resource.deleteConfirmTitle + "</strong></span>",
            content: resource.deleteConfirmText + ' ' + name + '?',
            buttons: '[' + resource.buttonCancel + '][' + resource.buttonDelete + ']'
        }, function (ButtonPressed) {
            if (ButtonPressed == resource.buttonDelete) {
                DeleteCompany(id);
            }
        });
    });

});

function DeleteCompany(id) {
    $.post(
        "/Company/Delete/",
        {
            id: id
        },
        function (data) {
            if (data === "DeleteSuccess") {
                $('#' + id).remove();
                $.smallBox({
                    title: resource.deleteSuccess,
                    content: "<i class='fa fa-clock-o'></i> <i>" + globalResource.smallBoxSuccessFooter + "</i>",
                    color: "#5F895F",
                    iconSmall: "fa fa-check bounce animated",
                    timeout: 4000
                });
            } else if (data === "DeleteError_NoCompany") {
                $.smallBox({
                    title: globalResource.smallBoxErrorTitle,
                    content: "<i class='fa fa-clock-o'></i> <i>" + resource.deleteError_NoCompany + "</i>",
                    color: "#C46A69",
                    iconSmall: "fa fa-times fa-2x fadeInRight animated",
                    timeout: 4000
                });
            } else if (data === "DeleteError_UsersInSystem") {
                $.smallBox({
                    title: globalResource.smallBoxErrorTitle,
                    content: "<i class='fa fa-clock-o'></i> <i>" + resource.deleteError_UsersInSystem + "</i>",
                    color: "#C46A69",
                    iconSmall: "fa fa-times fa-2x fadeInRight animated",
                    timeout: 4000
                });
            } else if (data === "DeleteError_ServiceObjectsInSystem") {
                GetServiceObjectsByCompanyID(id);
            }


        }
    );
}

function GetServiceObjectsByCompanyID(id) {
    $.post(
        "/Company/GetServiceObjectsByCompanyID/",
        {
            id: id
        },
        function (data) {
            $.smallBox({
                title: globalResource.smallBoxErrorTitle,
                content: "<i class='fa fa-clock-o'></i> <i>" + resource.deleteError_ServiceObjectsInSystem + ': ' + data + "</i>",
                color: "#C46A69",
                iconSmall: "fa fa-times fa-2x fadeInRight animated",
                timeout: 4000
            });
        }
    );
}