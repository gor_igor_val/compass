﻿const min = 3,
max = 30;

function validateName(inputName) {
    if (inputName.length > max || inputName.length < min) {
        return false;
    } else {
        return true;
    }
}

//Конфигурация таблицы
var tableOption = {
    "language": {
        "url": resource.tableLang
    },
}

var table = $('#eqipmentCategoryTable').DataTable(tableOption);

function AddCategory() {
    $.SmartMessageBox({
        title: "<span class='txt-color-orangeDark'><strong> " + resource.newTitle + "</strong></span>",
        content: resource.newText,
        buttons: "[" + resource.buttonCancel + "][" + resource.buttonAdd + "]",
        input: "text",
        placeholder: resource.newPlaceholder,
        inputValue: ""
    }, function (ButtonPress, Value) {
        if (ButtonPress === resource.buttonAdd) {
            var valid = validateName(Value);

            if (valid) {
                $.post(
                    "/Equipment/CreateCategory/",
                    {
                        name: Value
                    },
                    function (data) {
                        var row = table.row.add([
                            Value,
                            "<a class='editCategory' href='#' onclick='EditCategory(this);' data-category-id='" + data + "' data-category-name='" + Value + "'><span class='glyphicon glyphicon-pencil'></span></a>",
                            "<a class='delCategory' href='#' onclick='DeleteCategory(this);' data-category-id='" + data + "' data-category-name='" + Value + "'><span class='glyphicon glyphicon-trash'></span></a>",
                        ]).draw().node();
                        $.smallBox({
                            title: resource.newSuccess,
                            content: "<i class='fa fa-clock-o'></i> <i>" + globalResource.smallBoxSuccessFooter + "</i>",
                            color: "#5F895F",
                            iconSmall: "fa fa-check bounce animated",
                            timeout: 4000
                        });
                    }
                );
            }
            else {
                $.smallBox({
                    title: globalResource.smallBoxErrorTitle,
                    content: "<i class='fa fa-clock-o'></i> <i>" + resource.newError1 + " " + min + " " + resource.newError2 + " " + max + " " + resource.newError3 + "</i>",
                    color: "#C46A69",
                    iconSmall: "fa fa-times fa-2x fadeInRight animated",
                    timeout: 4000
                });
            }
        }
    });
} //end AddCategory

function EditCategory(obj) {
    var link = $(obj);
    var selId = link.attr('data-category-id');
    var oldName = link.attr('data-category-name');

    $.SmartMessageBox({
        title: "<span class='txt-color-orangeDark'><strong> " + resource.editTitle + "</strong></span>",
        content: resource.editText,
        buttons: "[" + resource.buttonCancel + "][" + resource.buttonSave + "]",
        input: "text",
        placeholder: resource.newPlaceholder,
        inputValue: oldName
    }, function (ButtonPress, Value) {
        if (ButtonPress === resource.buttonSave) {
            var valid = validateName(Value);

            if (valid) {
                $.post(
                    "/Equipment/EditCategory/",
                    {
                        Id: selId,
                        Name: Value
                    },
                    function (data) {
                        var row = table.row.add([
                            Value,
                            "<a class='editCategory' href='#' onclick='EditCategory(this);' data-category-id='" + selId + "' data-category-name='" + Value + "'><span class='glyphicon glyphicon-pencil'></span></a>",
                            "<a class='delCategory' href='#' onclick='DeleteCategory(this);' data-category-id='" + selId + "' data-category-name='" + Value + "'><span class='glyphicon glyphicon-trash'></span></a>",
                        ]).draw(false);
                        $.smallBox({
                            title: resource.editSuccess,
                            content: "<i class='fa fa-clock-o'></i> <i>" + globalResource.smallBoxSuccessFooter + "</i>",
                            color: "#5F895F",
                            iconSmall: "fa fa-check bounce animated",
                            timeout: 4000
                        });
                        $('#' + selId).remove();
                    }
                );
            }
            else {
                $.smallBox({
                    title: globalResource.smallBoxErrorTitle,
                    content: "<i class='fa fa-clock-o'></i> <i>" + resource.newError1 + " " + min + " " + resource.newError2 + " " + max + " " + resource.newError3 + "</i>",
                    color: "#C46A69",
                    iconSmall: "fa fa-times fa-2x fadeInRight animated",
                    timeout: 4000
                });
            }
        }
    });
} //end EditCategory

function DeleteCategory(obj) {
    var link = $(obj);
    var selName = link.attr('data-category-name');
    var selId = link.attr('data-category-id');

    $.SmartMessageBox({
        title: "<i class='fa fa-warning txt-color-orangeDark'></i><span class='txt-color-orangeDark'><strong> " + resource.deleteConfirmTitle + "</strong></span>",
        content: resource.deleteText + " \"" + ' ' + selName + '\"?',
        buttons: '[' + resource.buttonCancel + '][' + resource.buttonDelete + ']'
    }, function (ButtonPressed) {
        if (ButtonPressed == resource.buttonDelete) {

            $.post(
                "/Equipment/DeleteCategory/",
                {
                    Id: selId,
                    Name: selName
                },
                function (data) {
                    $('#' + selId).remove();
                    $.smallBox({
                        title: resource.deleteSuccess,
                        content: "<i class='fa fa-clock-o'></i> <i>" + globalResource.smallBoxSuccessFooter + "</i>",
                        color: "#5F895F",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    $('#Delete_Equipment_Category').prop('disabled', true)
                    $('#Edit_Equipment_Category').prop('disabled', true);

                }
            );
            

        }
    });
} //end DeleteCategory

$(document).ready(function () {
    $('#eqipmentCategoryTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $('#Delete_Equipment_Category').prop('disabled', true);
            $('#Edit_Equipment_Category').prop('disabled', true);
            $(this).removeClass('selected');
        }
        else {
            var categoryId = $(this).attr('id');
            var categoryName = $(this).attr('data-category-name');
            $('#Delete_Equipment_Category').attr('data-category-id', categoryId).attr('data-category-name', categoryName);
            $('#Edit_Equipment_Category').attr('data-category-id', categoryId).attr('data-category-name', categoryName);
            $('#Delete_Equipment_Category').prop('disabled', false)
            $('#Edit_Equipment_Category').prop('disabled', false);
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    $("#addCategory").on("click", function (e) {
        e.preventDefault();
        AddCategory();
    });

    $(".delCategory").on("click", function (e) {
        e.preventDefault();
        DeleteCategory(this);
    }); 

    $(".editCategory").on("click", function (e) {
        e.preventDefault();
        EditCategory(this);
    });

});