﻿var modalEdit = 0;
var _modalCompanies = $("#ModalCompanies");
var _companies = $("#Companies");
var _modalObjects = $("#ModalObjects");
var _objects = $("#Objects");
var _equipment = $("#ModalEquipment");
var _btnDelete = $("#btnDelete");

$(document).ready(function () {
    setTimeout(function () { $('body').removeClass('fadeInDown') }, 2000);

    // Validation
    var $validator = $("#modalForm").validate({
        // Rules for form validation
        rules: {
            ModalCompanies: {
                required: true
            },
            ModalObjects: {
                required: true
            },
            ModalEquipment: {
                required: true
            },
            startdate: {
                required: true,
            },
        },

        // Messages for form validation
        messages: {
            ModalCompanies: {
                required: resource.errorCompany,
            },
            ModalObjects: {
                required: resource.errorObject,
            },
            ModalEquipment: {
                required: resource.errorEquipment,
            },
            startdate: {
                required: resource.errorDate,
            },
        },

        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

    var hdr = {
        left: 'title',
        center: 'month,agendaWeek,agendaDay',
        right: 'prev,today,next'
    };

    /* initialize the calendar
 -----------------------------------------------------------------*/
    $('#calendar').fullCalendar({

        header: hdr,
        buttonText: {
            prev: '<i class="fa fa-chevron-left"></i>',
            next: '<i class="fa fa-chevron-right"></i>'
        },

        selectable: true,
        droppable: false, // this allows things to be dropped onto the calendar !!!
        timezone: 'local',
        eventLimit: true, // allow "more" link when too many events
        displayEventTime: false,

        select: function (start, end) {
            _modalCompanies.html(_companies.html());
            _modalCompanies.val(_companies.val());
            _modalObjects.html(_objects.html());
            _modalObjects.val(_objects.val());
            selectObject(_modalObjects);
            $("#ModalText").val("");
            $('#startdate').val(moment(start).format("DD.MM.YYYY"));
            $('#remember').prop('checked', true);
            var _radio = $('input:radio[name=priority]').first();
            _radio.click();
            modalEdit = 0;
            _btnDelete.hide();

            showModal();
        },

        eventClick: function (calEvent, jsEvent, view) {
            _modalCompanies.html(_companies.html());
            _modalCompanies.val(calEvent.companyId);
            _modalObjects.html(_objects.html());
            _modalObjects.val(calEvent.objectId);
            selectObject(_modalObjects);
            _equipment.val(calEvent.equipmentId);
            $("#ModalText").val(calEvent.text);
            $('#startdate').val(moment(calEvent.start).format("DD.MM.YYYY"));
            if (calEvent.remember == 'False') {
                $('#remember').prop('checked', false);
            } else {
                $('#remember').prop('checked', true);
            }
            var _radios = $('input:radio[name=priority]');
            var _radio = _radios.filter(function () { return this.value === String(calEvent.className).replace(/\,/g, ' ').trim() });
            _radio.click();
            modalEdit = calEvent.id;
            _btnDelete.show();

            showModal();
        },

        windowResize: function (event, ui) {
            $('#calendar').fullCalendar('render');
        }
    });

    /* hide default buttons */
    $('.fc-header-right, .fc-header-center').hide();

    $('#calendar-buttons #btn-prev').click(function () {
        $('.fc-button-prev').click();
        return false;
    });

    $('#calendar-buttons #btn-next').click(function () {
        $('.fc-button-next').click();
        return false;
    });

    $('#calendar-buttons #btn-today').click(function () {
        $('.fc-button-today').click();
        return false;
    });

    /* -----------------------------------------------------------------*/
    
    $("#Companies").on("change", (function () {
        selectCompany(_companies);
        GetEvents();
    }));

    $("#Objects").on("change", (function () {
        GetEvents();
    }));

    $("#ModalCompanies").on("change", (function () {
        selectCompany(_modalCompanies);
    }));

    $("#ModalObjects").on("change", (function () {
        selectObject(_modalObjects);
    }));

    GetEvents();
    $('#startdate').datepicker({
        dateFormat: 'dd.mm.yy',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
    });

    function showModal()
    {
        var el = $('.smart-form');
        el.find("em.invalid").hide();
        el.find(".state-error").removeClass("state-error");
        el.find(".state-success").removeClass("state-success");

        $('#myModal').modal({ backdrop: "static" })
    }

    // add or edit event   
    $('#btnOk').on('click', function (e) {
        // We don't want this to act as a link so cancel the link action
        e.preventDefault();
        var $valid = $("#modalForm").valid();
        if (!$valid) {
            $validator.focusInvalid();
            return false;
        }

        var _equipmentId = _equipment.val();
        var _text = $('#ModalText').val();
        var _serviceDate = $('#startdate');
        var _remember = $('#remember').is(":checked");
        var _color = $('input:radio[name=priority]:checked').val().trim();
        var _url = modalEdit == 0 ? '/ServiceCalendar/EventCreate' : '/ServiceCalendar/EventEdit';
        var _params = modalEdit == 0 
            ? JSON.stringify({
                EquipmentId: _equipmentId,
                Text: _text,
                ServiceDate: _serviceDate.datepicker("getDate"),
                Remember: _remember,
                CssClasses: _color
            })
            : JSON.stringify({
                Id: modalEdit,
                EquipmentId: _equipmentId,
                Text: _text,
                ServiceDate: _serviceDate.datepicker("getDate"),
                Remember: _remember,
                CssClasses: _color
            });

        $.ajax({                //аякс запрос
            url: _url,
            type: 'POST',
            contentType: 'application/json',
            dataType: "json",
            data: _params,
            success: function (result) {        //при успешном возврате с сервера
                if (result["Result"] == "OK") {
                    if (modalEdit != 0) {
                        $('#calendar').fullCalendar('removeEvents', modalEdit);
                    }
                    var eventData;
                    eventData = {
                        id: result["Id"],
                        title: $('#ModalEquipment option:selected').text().toString().trim(),
                        start: _serviceDate.datepicker("getDate"),
                        text: _text,
                        className: _color,
                        companyId: _modalCompanies.val(),
                        objectId: _modalObjects.val(),
                        equipmentId: _equipment.val(),
                        remember: _remember
                    };
                    $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                    $('#myModal').modal('hide');
                    $('#calendar').fullCalendar('unselect');
                    $.smallBox({
                        title: resource.successAdd,
                        content: "<i class='fa fa-clock-o'></i> <i>" + globalResource.smallBoxSuccessFooter + "</i>",
                        color: "#5F895F",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });

                }
                else {
                    alert('Error1: ' + result["Message"]);
                }
            },
            error: function (xhr) {
                alert('Error2: ' + xhr.statusText);
            },
        });
    });
    //end add or edit event

    // delete event   
    $('#btnDelete').on('click', function (e) {
        // We don't want this to act as a link so cancel the link action
        e.preventDefault();

        $.SmartMessageBox({
            title: "<i class='fa fa-warning txt-color-orangeDark'></i><span class='txt-color-orangeDark'><strong>" + resource.deleteConfirmTitle + "</strong></span>",
            content: resource.deleteConfirmText,
            buttons: '[' + resource.buttonCancel + '][' + resource.buttonDelete + ']'

        }, function (ButtonPressed) {
            if (ButtonPressed == resource.buttonDelete) {
                $.ajax({                //аякс запрос
                    url: '/ServiceCalendar/EventDelete',
                    type: 'POST',
                    contentType: 'application/json',
                    dataType: "json",
                    data: JSON.stringify({
                        Id: modalEdit,
                    }),
                    success: function (result) {        //при успешном возврате с сервера
                        if (result["Result"] == "OK") {
                            $('#calendar').fullCalendar('removeEvents', modalEdit);
                            $('#myModal').modal('hide');
                            $('#calendar').fullCalendar('unselect');
                            $.smallBox({
                                title: resource.successDelete,
                                content: "<i class='fa fa-clock-o'></i> <i>" + globalResource.smallBoxSuccessFooter + "</i>",
                                color: "#5F895F",
                                iconSmall: "fa fa-check bounce animated",
                                timeout: 4000
                            });

                        }
                        else {
                            alert('Error1: ' + result["Message"]);
                        }
                    },
                    error: function (xhr) {
                        alert('Error2: ' + xhr.statusText);
                    },
                });
            }
        });
    });
    //end delete event

});

function selectCompany(obj) {
    if (obj.attr("id") == "Companies") {
        var object = _objects;
    } else {
        var object = _modalObjects;
        _equipment.find("option:gt(0)").remove();
    }
    object.find("option:gt(0)").remove();
    $.ajax({                //аякс запрос
        url: '/ServiceCalendar/ChangeCompany',
        type: 'POST',
        contentType: 'application/json',
        dataType: "json",
        data: JSON.stringify({
            companyId: obj.val(),
        }),
        success: function (result) {        //при успешном возврате с сервера
            if (result["Result"] == "OK") {
                var _objects = result["Objects"];
                for (i = 0; i < _objects.length; i++) {
                    object.append($("<option></option>").attr("value", _objects[i].Id).text(_objects[i].Name));
                }
            }
            else {
                alert('Error1: ' + result["Message"]);
            }
        },
        error: function (xhr) {
            alert('Error2: ' + xhr.statusText);
        },
    });
}

function selectObject(obj) {
    _equipment.find("option:gt(0)").remove();
    if (!obj.val())
        return false;
    $.ajax({                //аякс запрос
        url: '/ServiceCalendar/ChangeObject',
        type: 'POST',
        contentType: 'application/json',
        dataType: "json",
        data: JSON.stringify({
            objectId: obj.val(),
        }),
        async: false,
        success: function (result) {        //при успешном возврате с сервера
            if (result["Result"] == "OK") {
                var _objects = result["Objects"];
                for (i = 0; i < _objects.length; i++) {
                    _equipment.append($("<option></option>").attr("value", _objects[i].Id).text(_objects[i].Name));
                }
            }
            else {
                alert('Error1: ' + result["Message"]);
            }
        },
        error: function (xhr) {
            alert('Error2: ' + xhr.statusText);
        },
    });
}

function GetEvents() {
    $('#calendar').fullCalendar('removeEvents');
    companyId = _companies.val();
    objectId = _objects.val();
    $.ajax({                //аякс запрос
        url: '/ServiceCalendar/GetEvents',
        type: 'POST',
        contentType: 'application/json',
        dataType: "json",
        data: JSON.stringify({
            companyId: companyId,
            objectId: objectId
        }),
        success: function (result) {        //при успешном возврате с сервера
            if (result["Result"] == "OK") {
                var _events = result["Events"];
                for (i = 0; i < _events.length; i++) {
                    $('#calendar').fullCalendar('renderEvent', _events[i], true);
                }
            }
            else {
                alert('Error1: ' + result["Message"]);
            }
        },
        error: function (xhr) {
            alert('Error2: ' + xhr.statusText);
        },
    });
}
