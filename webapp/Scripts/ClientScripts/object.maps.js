/**
 * Created by Oleg on 22.05.2017.
 */
var marker;
var infoWindow;

function geocodeAddress(geocoder, searchAddress, callback) {
    geocoder.geocode({ "address": searchAddress }, function (results, status) {
        if (status === "OK") {
            var location = results[0].geometry.location;
            var formattedAddress = results[0].formatted_address;
            callback({
                "location": location,
                "address": formattedAddress
            });
        } else {
            alert(resource.mapError1 + ": " + status);
        }
    });
}

function reverseGeocoding(geocoder, location, callback) {
    geocoder.geocode({ "location": location }, function (results, status) {
        if (status === "OK") {
            callback(results[0].address_components, results[0].formatted_address);
        } else {
            alert(resource.mapError2 + ": " + status);
        }
    });
}

function addGeocodeButton(geocoder, map) {
    var button = document.createElement("BUTTON");
    button.classList.add("btn", "btn-primary");
    button.id = "geocode-btn";
    //var buttonText = document.createTextNode(resource.mapButton);
    var buttonText = document.createElement("div");
    buttonText.innerHTML = resource.mapButton;
    button.appendChild(buttonText);
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(button);

    button.addEventListener("click", function (event) {
        event.preventDefault();

        var address = getAddress();
        if (address !== null) {
            geocodeAddress(geocoder, address, function (result) {
                placeMarker(map, result.location, result.address);
            });
        }
    });
}

function getAddress() {
    var oblast = document.getElementById('Oblast').value;
    //var district = document.getElementById('District').value;
    var city = document.getElementById('City').value;
    var street = document.getElementById('Street').value;
    var house = document.getElementById('House').value;

    oblast = oblast ? " " + oblast + " " + resource.mapOblast : "";
    city = city ? " " + city + "," : "";
    street = street ? street + " " + resource.mapStreet + "," : "";
    house = house ? " " + house + "," : "";

    if (oblast || city || street || house) {
        var address = (street + house + city + oblast).trim();
        return address;
    }
    alert(resource.mapError3);
    return null;
}

function setAddress(address) {
    var oblast = getAddressComponent(address, "administrative_area_level_1");
    var district = getAddressComponent(address, "administrative_area_level_3");
    var city = getAddressComponent(address, "locality");
    var street = getAddressComponent(address, "route");
    var house = getAddressComponent(address, "street_number");

    oblast = oblast ? oblast.replace(resource.mapOblast, "").trim() : "";
    district = district ? district : "";
    city = city ? city : "";
    street = street ? street.replace(resource.mapStreet, "").trim() : "";
    house = house ? house : "";

    document.getElementById('Oblast').value = oblast;
    document.getElementById('District').value = district;
    document.getElementById('City').value = city;
    document.getElementById('Street').value = street;
    document.getElementById('House').value = house;
}

function getAddressComponent(addressComponents, componentName) {
    for (var i = 0; i < addressComponents.length; i++) {
        if (addressComponents[i].types.includes(componentName)) {
            return addressComponents[i].long_name;
        }
    }
    return null;
}

function placeMarker(map, position, address) {
    if (marker !== undefined) {
        marker.setMap(null);
    }

    //map.setCenter(position);
    //map.setZoom(15);

    marker = new google.maps.Marker({
        position: position,
        map: map
    });
    infoWindow = new google.maps.InfoWindow({
        content: resource.mapLat + ": " + position.lat() + "<br>" + resource.mapLng + ": " + position.lng() + "<br>" + resource.mapAdress + ": " + address
    });
    infoWindow.open(map, marker);

    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.open(map, marker);
    });
}