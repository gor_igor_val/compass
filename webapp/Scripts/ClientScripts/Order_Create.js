﻿var handleJqueryFileUpload = function () {
    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        autoUpload: true,
        disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
        maxFileSize: 5000000,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|doc?x)$/i,
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},                
    });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    // Upload server status check for browsers with CORS support:
    if ($.support.cors) {
        $.ajax({
            type: 'HEAD'
        }).fail(function () {
            $('<div class="alert alert-danger"/>').text('Upload server currently unavailable - ' + new Date()).appendTo('#fileupload');
        });
    }

    if (isEdit) {
        $('#fileupload').addClass('fileupload-processing');
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: '/Order/GetFileList',
            dataType: "json",
            data: JSON.stringify({ path: $('#DocumentLink').val() }),
            success: function (data) {
                $('#fileupload').fileupload('option', 'done').call($('#fileupload'), $.Event('done'), { result: { files: data.files } })
                $('#fileupload').removeClass('fileupload-processing');
            }
        });
    }

};

var FormMultipleUpload = function () {
    "use strict";
    return {
        //main function
        init: function () {
            handleJqueryFileUpload();
        }
    };
}();


$(document).ready(function () {
    var sectionPerformer = $("#sectionPerformer");
    var sectionQuality = $("#sectionQuality");
    if (parseInt($("#OrderStatus").val()) < 3) {
        sectionPerformer.hide();
    }
    else {
        sectionPerformer.show();
    }
    if (parseInt($("#OrderStatus").val()) != 6) {
        sectionQuality.hide();
    }
    else {
        sectionQuality.show();
    }

    // Validation
    var $validator = $("#form").validate({
        // Rules for form validation
        rules: {
            Company: {
                required: true
            },
            Object: {
                required: true
            },
            Client: {
                required: true
            },
            Administrator: {
                required: true,
            },
            Name: {
                required: true,
                maxlength: 100
            },
            Content: {
                required: true,
                maxlength: 500
            },
            OrderType: {
                required: true
            },
            OrderStatus: {
                required: true
            },
            Process: {
                maxlength: 500
            },
            Quality: {
                min: 1,
                max: 5
            },
            Escalation: {
                min: 1,
                max: 5
            },
        },

        // Messages for form validation
        messages: {
            Company: {
                required: resource.errorCompany,
            },
            Object: {
                required: resource.errorObject,
            },
            Client: {
                required: resource.errorClient,
            },
            Administrator: {
                required: resource.errorAdmin,
            },
            Name: {
                required: resource.errorName1,
                maxlength: resource.errorName2,
            },
            Content: {
                required: resource.errorContent1,
                maxlength: resource.errorContent2,
            },
            OrderType: {
                required: resource.errorType,
            },
            OrderStatus: {
                required: resource.errorStarus,
            },
            Process: {
                maxlength: resource.errorProcess,
            },
            Quality: {
                min: resource.qualityError,
                max: resource.qualityError
            },
            Escalation: {
                min: resource.qualityError,
                max: resource.qualityError
            },
        },

        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

    $("#Company").on("change", (function () {
        selectCompany(this);
    }));

    $("#Object").on("change", (function () {
        selectObject(this);
    }));

    $("#OrderStatus").on("change", (function () {
        if ($(this).val() == 6) { //Прийнято
            sectionQuality.show();
        } else {
            sectionQuality.hide();
        }

        if ($(this).val() >= 3) { //Призначений виконувач
            sectionPerformer.show()
        } else {
            sectionPerformer.hide();
        }
        }));

    $("#submit-form").on("click", (function () {
        var $valid = $("#form").valid();
        if (!$valid) {
            $validator.focusInvalid();
            return false;
        }
        $('#form')[0].submit();
    }));

    $("#Performer").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Order/PerformerAutocomplete",
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }))

                }
            })
        },
        messages: {
            noResults: "",
            results: function (resultsCount) { }
        }
    });
});

function selectCompany(obj) {
    var object = $("#Object");
    var client = $("#Client");
    if (!obj.value) {
        object.prop("disabled", true).find('option[value!=""]').remove();
        object.closest('label.select').addClass('state-disabled');
        client.find("option:gt(0)").remove();
        return false;
    }

    $.ajax({                //аякс запрос
        url: '/Order/ChangeCompany',
        type: 'POST',
        contentType: 'application/json',
        dataType: "json",
        data: JSON.stringify({
            companyId: obj.value,
        }),
        success: function (result) {        //при успешном возврате с сервера
            if (result["Result"] == "OK") {
                var _objects = result["Objects"];
                object.prop("disabled", false).find('option[value!=""]').remove();
                object.closest('label.state-disabled').removeClass('state-disabled');
                for (i = 0; i < _objects.length; i++) {
                    object.append($("<option></option>").attr("value", _objects[i].Id).text(_objects[i].Name));
                }

                var _clients = result["Users"];
                client.find('option').remove();
                for (i = 0; i < _clients.length; i++) {
                    client.append($("<option></option>").attr("value", _clients[i].Id).text(_clients[i].FullName));
                }
            }
            else {
                alert('Error1: ' + result["Message"]);
            }
        },
        error: function (xhr) {
            alert('Error2: ' + xhr.statusText);
        },
    });

}

function selectObject(obj) {
    var admin = $("#Administrator");
    var equipment = $("#Equipment");
    if (!obj.value) {
        admin.prop("disabled", true).find('option').remove();
        admin.append($("<option></option>").attr("value", "").text(resource.adminDefaultText));
        admin.closest('label.select').addClass('state-disabled');
        equipment.prop("disabled", true).find('option[value!=""]').remove();
        equipment.closest('label.select').addClass('state-disabled');
        return false;
    }

    $.ajax({                //аякс запрос
        url: '/Order/ChangeObject',
        type: 'POST',
        contentType: 'application/json',
        dataType: "json",
        data: JSON.stringify({
            objectId: obj.value,
        }),
        success: function (result) {        //при успешном возврате с сервера
            if (result["Result"] == "OK") {
                if (result["Admin"]) {
                    admin.prop("disabled", false).find('option').remove();
                    admin.append($("<option></option>").attr("value", result["Admin"].Id).text(result["Admin"].FullName));
                    admin.closest('label.state-disabled').removeClass('state-disabled');
                }
                else {
                    admin.find('option').remove();
                    admin.append($("<option></option>").attr("value", "").text(resource.adminDefaultText));
                }

                var _equipments = result["Equipments"];
                equipment.prop("disabled", false).find('option[value!=""]').remove();
                equipment.closest('label.state-disabled').removeClass('state-disabled');
                for (i = 0; i < _equipments.length; i++) {
                    equipment.append($("<option></option>").attr("value", _equipments[i].Id).text(_equipments[i].Name));
                }
            }
            else {
                alert('Error1: ' + result["Message"]);
            }
        },
        error: function (xhr) {
            alert('Error2: ' + xhr.statusText);
        },
    });
}