﻿//Осуществить асинхронный запрос на удаление объекта с указаным Id
function DeleteObject(id) {
    $.post(
    "/Object/Delete/",
    {
        id: id
    },
    function (data) {
        if (data === "DeleteSuccess") {
            $('#' + id).remove();
            $.smallBox({
                title: resource.deleteSuccess,
                content: "<i class='fa fa-clock-o'></i> <i>" + globalResource.smallBoxSuccessFooter + "</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
        } else if (data === "DeleteError") {
            $.smallBox({
                title: globalResource.smallBoxErrorTitle,
                content: "<i class='fa fa-clock-o'></i> <i>" + resource.deleteError + "</i>",
                color: "#C46A69",
                iconSmall: "fa fa-times fa-2x fadeInRight animated",
                timeout: 4000
            });
        } else if (data === "DeleteErrorEquipment") {
            $.smallBox({
                title: globalResource.smallBoxErrorTitle,
                content: "<i class='fa fa-clock-o'></i> <i>" + resource.DeleteErrorEquipment + "</i>",
                color: "#C46A69",
                iconSmall: "fa fa-times fa-2x fadeInRight animated",
                timeout: 4000
            });
        }
    });
};

//Конфигурация таблицы
var tableOption = {
    "language": {
        "url": resource.tableLang
    },
    'columnDefs': [
        {
            "targets": "no-sort",
            "bSortable": false
        }
    ]
}

$(document).ready(function () {
    var table = $('#objectsTable').dataTable(tableOption);

    $('#objectsTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $('#DeleteObject').hide();
            $('#EditObject').hide();
            $('#ViewEquipment').hide();
            $(this).removeClass('selected');
        }
        else {
            var objectId = $(this).attr('id');
            var objectName = $(this).attr('data-object-name');
            var id = $(this).attr('id');
            $('#DeleteObject').attr('data-object-id', objectId).attr('data-object-name', objectName);
            $('#EditObject').attr('action', "/Object/Edit/" + objectId);
            $('#ViewEquipment').attr('action', "/equipment/index/" + id);
            $('#DeleteObject').show();
            $('#EditObject').show();
            $('#ViewEquipment').show();
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    $(".viewDialog").on("click", function (e) {
        e.preventDefault();

        var name = $(this).attr('data-object-name');
        var id = $(this).attr('data-object-id');

        $.SmartMessageBox({
            title: "<i class='fa fa-warning txt-color-orangeDark'></i><span class='txt-color-orangeDark'><strong> " + resource.deleteConfirmTitle + "</strong></span>",
            content: resource.deleteConfirmText + ' ' + name + '?',
            buttons: '[' + resource.buttonCancel + '][' + resource.buttonDelete + ']'
        }, function (ButtonPressed) {
            if (ButtonPressed == resource.buttonDelete) {
                DeleteObject(id);
            }
        });

    });
});