﻿var tableOption = {
    "language": {
        "url": resource.tableLang
    },
    "scrollX": true,
    'columnDefs': [
        {
            "targets": "no-sort",
            "bSortable": false
        }
    ],
    'order': [[ 0, "desc" ]]
};

$(document).ready(function () {
    var table = $('#orderTable').DataTable(tableOption);

    $('#orderTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $('#editOrderButton').prop("disabled", true);
            $(this).removeClass('selected');
        }
        else {
            var orderId = $(this).attr('id');
            $('#editOrderForm').attr('action', "/Order/Edit/" + orderId);
            $('#editOrderButton').prop("disabled", false);
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    // Apply the filter
    $("#orderTable thead th input[type=text]").on('keyup change', function () {

        table
            .column($(this).parent().index() + ':visible')
            .search(this.value)
            .draw();

    });

    $(".dropdowngroup").on('change', function () {
        var text = "";
        if (this.value) {
            text = $("option:selected", this).text().trim();
        }
        table
            .column($(this).parent().parent().index() + ':visible')
            .search(text)
            .draw();

    });

});