﻿//Осуществить асинхронный запрос на удаление пользователя с указаным Id
function DeleteUser(id) {
    $.post(
        "/Users/Delete/",
        {
            id: id
        },
        function (data) {
            if (data === "DeleteSuccess") {
                $('#' + id).remove();
                $.smallBox({
                    title: resource.deleteSuccess,
                    content: "<i class='fa fa-clock-o'></i> <i>" + globalResource.smallBoxSuccessFooter + "</i>",
                    color: "#5F895F",
                    iconSmall: "fa fa-check bounce animated",
                    timeout: 4000
                });
            } else if (data === "DeleteError") {
                $.smallBox({
                    title: globalResource.smallBoxErrorTitle,
                    content: "<i class='fa fa-clock-o'></i> <i>" + resource.deleteError + "</i>",
                    color: "#C46A69",
                    iconSmall: "fa fa-times fa-2x fadeInRight animated",
                    timeout: 4000
                });
            }
        }
    );
};

//Конфигурация таблицы
var tableOption = {
    "language": {
        "url": resource.tableLang
    },
    'columnDefs': [
        {
            "targets": "no-sort",
            "bSortable": false
        }
    ]
}

$(document).ready(function () {
    var table = $('#usersTable').dataTable(tableOption);

    $('#usersTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $('#DeleteUser').hide();
            $('#EditUser').hide();
            $(this).removeClass('selected');
        }
        else {
            var userId = $(this).attr('id');
            var userName = $(this).attr('data-user-login');
            $('#DeleteUser').attr('data-user-id', userId).attr('data-user-login', userName);
            $('#EditUser').attr('action', "/Users/Edit/" + userId);
            $('#DeleteUser').show();
            $('#EditUser').show();
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    $(".viewDialog").on("click", function (e) {
        e.preventDefault();

        var login = $(this).attr('data-user-login');
        var id = $(this).attr('data-user-id');

        $.SmartMessageBox({
            title: "<i class='fa fa-warning txt-color-orangeDark'></i><span class='txt-color-orangeDark'><strong> " + resource.deleteConfirmTitle + "</strong></span>",
            content: resource.deleteConfirmText + ' ' + login + '?',
            buttons: '[' + resource.buttonCancel + '][' + resource.buttonDelete + ']'
        }, function (ButtonPressed) {
            if (ButtonPressed == resource.buttonDelete) {
                DeleteUser(id);
            }
        });

    });
});