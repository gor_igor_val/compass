﻿$(document).ready(function () {
    //phone mask
    $("#Number").mask("(099) 999-9999");

    // Validation
    $("#form").validate({
        // Rules for form validation
        rules: {
            Name: {
                required: true
            },
            Adress: {
                required: true
            },
            ContactPerson: {
                required: true
            },
            Email: {
                required: true,
                email: true
            },
            NeedValidation: {
                required: true
            },
            Number: {
                required: true
            },
        },

        // Messages for form validation
        messages: {
            Name: {
                required: resource.errorName,
            },
            Adress: {
                required: resource.errorAdress,
            },
            ContactPerson: {
                required: resource.errorContact,
            },
            Email: {
                required: resource.errorEmail1,
                email: resource.errorEmail2
            },
            NeedValidation: {
                required: resource.errorValidation,
            },
            Number: {
                required: resource.errorPhone,
            },
        },

        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

    $(".quality-param").each(function () {
        $(this).rules('add', {
            number: true,
            required: true,
            messages: {
                required: resource.errorParam,
            },
        });
    });

});

function progress(html) {
    document.getElementById('progress').innerHTML = html;
}

document.forms.form.onsubmit = function () {
    var file = this.elements.upload.files[0];
    if (file) {
        uploads(file);
        document.forms.form.onsubmit;
    }
    return document.forms.form.onsubmit;
}

function uploads(file) {

    var xhr = new XMLHttpRequest();

    // обработчики можно объединить в один,
    // если status == 200, то это успех, иначе ошибка
    xhr.onload = xhr.onerror = function () {
        if (this.status == 200 || this.status == 0) {
            progress(resource.loadDone);
        } else {
            progress(resource.loadError + " " + this.status);
        }
    };

    // обработчик для закачки
    xhr.upload.onprogress = function (event) {
        progress(resource.loadProgress);
    }

    if (isEdit) {
        var id = $("#Id").val();
        xhr.open("POST", "/Company/Edit/" + id, true);
    } else {
        xhr.open("POST", "/Company/Create", true);
    }
    xhr.send(file);
}

