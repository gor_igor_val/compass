﻿$(document).ready(function () {
    //phone mask
    $("#Phone").mask("(099) 999-9999");

    // Validation
    $("#form").validate({
        // Rules for form validation
        rules: {
            UserName: {
                required: true
            },
            FirstName: {
                required: true
            },
            LastName: {
                required: true
            },
            Email: {
                required: true,
                email: true
            },
            Role: {
                required: true
            },
            Password: {
                required: true,
                minlength: 6,
                maxlength: 20
            },
            PasswordConfirm: {
                required: true,
                minlength: 6,
                maxlength: 20,
                equalTo: '#Password'
            },
            RegionId: {
                required: true,
                remote: "/Users/CheckManagerIsExists/"
            }
        },

        // Messages for form validation
        messages: {
            UserName: {
                required: resource.errorLogin
            },
            FirstName: {
                required: resource.errorFirstName
            },
            LastName: {
                required: resource.errorLastName
            },
            Email: {
                required: resource.errorEmail1,
                email: resource.errorEmail2
            },
            Role: {
                required: resource.errorRole
            },
            Password: {
                required: resource.errorPassword1,
                minlength: jQuery.validator.format(resource.errorPassword2)
            },
            PasswordConfirm: {
                required: resource.errorConfirmPassword1,
                equalTo: resource.errorConfirmPassword2,
                minlength: jQuery.validator.format(resource.errorPassword2)
            },
            RegionId: {
                required: resource.errorRegion,
                remote: resource.errorManagerExist
            }
        },

        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });

    if (isEdit) {
        $('#Role').rules('remove', 'required');
        $('#Password').rules('remove', 'required');
        $('#PasswordConfirm').rules('remove', 'required');

        $("#RegionId").rules("remove", "remote");
    }

    //$(".chzn-select").chosen('destroy');

    //Изначально делаем все списки множественного выбора chosen
    $(".chzn-select").chosen();

    //Скрываем те поля, что зависят от роли, если это создание пользователя
    if (!isEdit) {
        $("#sel-company").hide();
        $("#sel-object").hide();
        $("#sel-region").hide();
        //Иначе если это редактирование, роль уже определена и нужно показать ее поля
    } else {
        onRoleChange(true);
    }

    //Смена роли
    $("#Role").on("change", function () {
        onRoleChange(false);
    });

    //Не понял для чего этот код ↓ !!
    //if ($("#submitButton").submit !== true) {
    //    //select();
    //    onRoleChange();
    //}

    function onRoleChange(firstChange) {
        //GUIDы ролей
        //const adminId = "2ba0b9d5-4af0-412b-99fb-80ca7a8349bc";
        //const validatorId = "7413283b-6766-4abc-8e9e-dd229ace5aae";
        //const initiatorId = "c934a64c-f04e-4bd6-a257-222a4ceca33c";
        //const dispatcherId = "eb3a2e9a-e6b5-46e9-b420-e115837e69c9";

        var role = $("#Role option:selected").text();

        //Уничтожить предыдущие объекты Chosen (Так как мы конфигурируем их заново)
        $(".chzn-select").chosen("destroy");
        //Очистить предыдущий выбор, если это не первый выбор
        if (!firstChange) {
            $(".chzn-select, #sel-region select").val("");
        }

        switch (role) {
            case resource.admin:
                //Региональный управляющий может выбирать организации и регион для управления
                $("#sel-company").show();
                $("#sel-object").hide();
                $("#sel-region").show();

                //Максимум 5 организаций
                $(".chzn-select").chosen({
                    max_selected_options: 5,
                    placeholder_text_single: resource.placeholder
                });

                break;

            case resource.validator:
                //Валидатор может выбирать организации и прикрепленные объекты
                $("#sel-company").show();
                $("#sel-object").show();
                $("#sel-region").hide();

                //Максимум 1 организация (родная для валидатора)...
                $("#sel-company .chzn-select").chosen({
                    max_selected_options: 1,
                    placeholder_text_single: resource.placeholder
                });
                //...и 1 прикрепленный объект
                $("#sel-object .chzn-select").chosen({
                    max_selected_options: 1,
                    placeholder_text_single: resource.placeholder
                });

                break;

            case resource.initiator:
                //Инициатор может выбирать организации и прикрепленные объекты
                $("#sel-company").show();
                $("#sel-object").show();
                $("#sel-region").hide();

                //Максимум 1 организация (родная для инициатора)...
                $("#sel-company .chzn-select").chosen({
                    max_selected_options: 1,
                    placeholder_text_single: resource.placeholder
                });
                //...и 5 прикрепленных объектов
                $("#sel-object .chzn-select").chosen({
                    max_selected_options: 5,
                    placeholder_text_single: resource.placeholder
                });

                break;

            case resource.dispatcher:
                //Диспетчер не может выбрать ничего :(
                $("#sel-company").hide();
                $("#sel-object").hide();
                $("#sel-region").hide();

                $(".chzn-select").chosen();

                break;

            default:
                //Иначе скрыть все поля, что зависят от роли
                $("#sel-company").hide();
                $("#sel-object").hide();
                $("#sel-region").hide();

                $(".chzn-select").chosen();
        }
    }
});

//function select() {
//    $(".chzn-select").chosen('destroy');
//    if ($("#Role option:selected").text() === resource.admin) {
//        $("#sel-company").show();
//        $("#sel-object").hide();
//        $(".chzn-select").chosen({
//            max_selected_options: 5,
//            placeholder_text_single: resource.placeholder
//        });
//        $(".chzn-select").val('').trigger("chosen:updated");
//    } else if ($("#Role option:selected").text() === resource.dispatcher) {
//        $("#sel-company").hide();
//        $("#sel-object").hide();
//        $(".chzn-select").chosen();
//        $(".chzn-select").val('').trigger("chosen:updated");
//    } else if ($("#Role option:selected").text() === resource.validator) {
//        $("#sel-company").show();
//        $("#sel-object").show();
//        $("#sel-company .chzn-select").chosen({
//            max_selected_options: 1,
//            placeholder_text_single: resource.placeholder
//        });
//        $("#sel-object .chzn-select").chosen({
//            max_selected_options: 1,
//            placeholder_text_single: resource.placeholder
//        });
//        $(".chzn-select").val('').trigger("chosen:updated");
//    } else if ($("#Role option:selected").text() === resource.initiator) {
//        $("#sel-company").show();
//        $("#sel-object").show();
//        $("#sel-company .chzn-select").chosen({
//            max_selected_options: 1,
//            placeholder_text_single: resource.placeholder
//        });
//        $("#sel-object .chzn-select").chosen({
//            max_selected_options: 5,
//            placeholder_text_single: resource.placeholder
//        });
//        $(".chzn-select").val('').trigger("chosen:updated");
//    } else {
//        $("#sel-company").show();
//        $("#sel-object").hide();
//        $(".chzn-select").chosen({
//            max_selected_options: 1,
//            placeholder_text_single: resource.placeholder
//        });
//        $(".chzn-select").val('').trigger("chosen:updated");
//    }
//}