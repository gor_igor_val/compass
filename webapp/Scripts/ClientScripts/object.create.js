﻿$(document).ready(function () {
    //phone mask
    $("#PhoneNumber").mask("(099) 999-9999");

    // Validation
    $("#form").validate({
        // Rules for form validation
        rules: {
            Name: {
                required: true
            },
            Company: {
                required: true
            },
            Region: {
                required: true
            },
            Oblast: {
                required: true
            },
            District: {
                required: true
            },
            City: {
                required: true
            },
            Street: {
                required: true
            },
            House: {
                required: true
            },
            ContactPerson: {
                required: true
            },
            Email: {
                email: true
            }
        },

        // Messages for form validation
        messages: {
            Name: {
                required: resource.errorName
            },
            Company: {
                required: resource.errorCompany
            },
            Region: {
                required: resource.errorRegion
            },
            Oblast: {
                required: resource.errorOblast
            },
            District: {
                required: resource.errorDistrict
            },
            City: {
                required: resource.errorCity
            },
            Street: {
                required: resource.errorStreet
            },
            House: {
                required: resource.errorHouse
            },
            ContactPerson: {
                required: resource.errorContact
            },
            Email: {
                email: resource.errorEmail
            }
        },

        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
});

if (isEdit) {
    function initMap() {
        var map = new google.maps.Map(document.getElementById("map"), {
            zoom: 7,
            center: { lat: 49.272, lng: 31.685 }
        });
        var geocoder = new google.maps.Geocoder();

        addGeocodeButton(geocoder, map);

        var address = getAddress();
        if (address !== null) {
            geocodeAddress(geocoder, address, function (result) {
                placeMarker(map, result.location, result.address);
            });
        }

        google.maps.event.addListener(map, 'click', function (event) {
            reverseGeocoding(geocoder, event.latLng, function (addressComponents, formattedAddress) {
                placeMarker(map, event.latLng, formattedAddress);
                setAddress(addressComponents);
            });
        });
    }
} else {
    function initMap() {
        const coordUkraine = new google.maps.LatLng(49.272, 31.685);
        var map = new google.maps.Map(document.getElementById("map"), {
            zoom: 6,
            center: coordUkraine
        });
        var geocoder = new google.maps.Geocoder();

        addGeocodeButton(geocoder, map);

        google.maps.event.addListener(map, "click", function (event) {
            reverseGeocoding(geocoder, event.latLng, function (addressComponents, formattedAddress) {
                placeMarker(map, event.latLng, formattedAddress);
                setAddress(addressComponents);
            });
        });
    }
}