﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Compass.Models
{
    public class OrderCreateViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_SelectCompany")]
        public int Company { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Order_Create_Required_Object")]
        public int Object { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Order_Required_ClientError")]
        public string Client { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Order_Required_AdminError")]
        public string Administrator { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Order_Required_NameError")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Order_Max_NameError")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Order_Required_ContentError")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Order_Max_ContentError")]
        public string Content { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Order_Required_TypeError")]
        public int OrderType { get; set; }

        public int? Equipment { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Order_Required_StatusError")]
        public int OrderStatus { get; set; }

        public short? Escalation { get; set; }

        [StringLength(500, ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Order_Max_ContentError")]
        public string Process { get; set; }

        public short? Quality { get; set; }
        
        public string DocumentLink { get; set; }

        public string Performer { get; set; }
    }

    public class UsersToSelect
    {
        public string Id { get; set; }
        public string FullName { get; set; }
    }

    public class ObjectsToSelect
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class OrderDetailsViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
        public string Company { get; set; }
        public string Object { get; set; }
        public string Client { get; set; }
        public string Administrator { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string OrderType { get; set; }
        public string Equipment { get; set; }
        public string OrderStatus { get; set; }
        public short? Escalation { get; set; }
        public string Process { get; set; }
        public short? Quality { get; set; }
        public string DocumentLink { get; set; }
        public string Performer { get; set; }
    }

    public class CalendarTable
    {
        public DateTime start { get; set; }
        public DateTime? end { get; set; }
        public string title { get; set; }
        public string id { get; set; }
        public string className { get; set; }
        public string text { get; set; }
        public string companyId { get; set; }
        public string objectId { get; set; }
        public string equipmentId { get; set; }
        public string remember { get; set; }
    }
}