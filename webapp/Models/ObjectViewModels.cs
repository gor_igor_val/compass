﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Compass.Models
{
    public class ObjectCreateViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Object_Required_EnterObjectName")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_SelectCompany")]
        public int Company { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Region_SelectRegion")]
        public int Region { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Address_Required_EnterOblast")]
        public string Oblast { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Address_Required_EnterDistrict")]
        public string District { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Address_Required_EnterCity")]
        public string City { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Address_Required_EnterStreet")]
        public string Street { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Address_Required_EnterHouse")]
        public string House { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Object_Required_EnterContactPerson")]
        public string ContactPerson { get; set; }

        [RegularExpression(@"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$", ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_PhoneInvalid")]
        public string PhoneNumber { get; set; }

        [EmailAddress(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_EmailInvalid")]
        public string Email { get; set; }
    }

    public class ObjectDetailsViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Company { get; set; }

        public string Region { get; set; }

        public string Oblast { get; set; }

        public string District { get; set; }

        public string City { get; set; }

        public string Street { get; set; }

        public string House { get; set; }

        public string ContactPerson { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }
    }
}