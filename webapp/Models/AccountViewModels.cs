﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Compass.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Код")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Запомнить браузер?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }
    }

    public class CreateViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Users_Create_Required_EnterLogin")]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_EnterFirstName")]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_EnterLastName")]
        public string LastName { get; set; }

        public string Patronymic { get; set; }

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$", ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_EnterPhoneNumber")]
        public string Phone { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_EnterEmail")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_EmailInvalid")]
        public string Email { get; set; }
        
        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Users_Create_Required_Password")]
        [DataType(DataType.Password)]
        // TODO: Настроить клиентскую валидацию для пароля, если он не соответствует шаблону
        //[RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{6,}", ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Users_Create_PasswordNotValid")]
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Users_Create_Required_PasswordConfirm")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Users_Create_Required_PasswordsNotMatch")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Users_Create_Required_SelectRole")]
        public string Role { get; set; }

        public List<string> Company { get; set; }

        public List<string> ServiceObject { get; set; }

        public int? RegionId { get; set; }
    }

    public class EditViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public string Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Users_Create_Required_EnterLogin")]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_EnterFirstName")]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_EnterLastName")]
        public string LastName { get; set; }

        public string Patronymic { get; set; }

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$", ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_EnterPhoneNumber")]
        public string Phone { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_EnterEmail")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_EmailInvalid")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{6,}", ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Users_Create_PasswordNotValid")]
        public string Password { get; set; }

        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Users_Create_Required_PasswordConfirm")]
        public string PasswordConfirm { get; set; }

        public string Role { get; set; }

        public List<string> Company { get; set; }

        public List<string> ServiceObject { get; set; }

        public int? RegionId { get; set; }
    }

    public class DetailsViewModel
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string Company { get; set; }
        public string ServiceObject { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Users_Create_Required_Password")]
        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{6,}", ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Users_Create_PasswordNotValid")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Users_Create_Required_PasswordConfirm")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Users_Create_Required_PasswordsNotMatch")]
        public string passwordConfirm { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
