﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Compass.Models
{
    public class CompanyCreateViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public string Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Company_Create_NameError")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Company_Create_AdressError")]
        public string Adress { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Company_Create_ContactError")]
        public string ContactPerson { get; set; }

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$", ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_EnterPhoneNumber")]
        [Required(ErrorMessageResourceType = typeof(Resources.TempResource), ErrorMessageResourceName = "UsersCreatePhone")]
        public string Number { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_EnterEmail")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Shared_Required_EmailInvalid")]
        public string Email { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Resources.TempResource), ErrorMessageResourceName = "FileUploadError")]
        public string LinkToFileAgreement { get; set; }

        /* [Required(ErrorMessage = "Введіть значення для параметрів якості")]
         [Display(Name = "Параметри якості")]
         public List<QualityParams> QualityParam { get; set; }*/

        [Required(ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Company_Create_ValidationError")]
        public bool? NeedValidation { get; set; }

        public string Agreement { get; set; }

        public List<QualityParams> QualityParams { get; set; }
    }

    public class CompanyDetailsViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Adress { get; set; }

        public string ContactPerson { get; set; }

        public string Number { get; set; }

        public string Email { get; set; }

        public string LinkToFileAgreement { get; set; }

        public List<QualityParams> QualityParam { get; set; }

        public string NeedValidation { get; set; }

        public string DownloadAgreement { get; set; }
    }

    public class DownloadAgreement
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
    }

    public class QualityParams
    {
        public int Id { get; set; }
        public string Param { get; set; }

        [Range(-32768, 32767, ErrorMessageResourceType = typeof(Resources.MainResource), ErrorMessageResourceName = "Company_Create_ParamValueError")]
        [Required(ErrorMessageResourceType = typeof(Resources.TempResource), ErrorMessageResourceName = "QualityParamsError")]
        public short? Value { get; set; }
    }

}