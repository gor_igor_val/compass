﻿#region Using

using System.Web.Optimization;

#endregion

namespace Compass
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/content/compass").IncludeDirectory("~/content/css", "*.css"));

            bundles.Add(new ScriptBundle("~/scripts/compass").Include(
                "~/scripts/app.config.js",
                "~/scripts/bootstrap/bootstrap.min.js",
                "~/scripts/notification/SmartNotification.min.js",
                "~/scripts/smartwidgets/jarvis.widget.min.js",
                "~/Scripts/jquery.unobtrusive-ajax.js",
                "~/scripts/app.js",
                "~/scripts/plugin/jquery-validate/jquery.validate.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/fileupload").Include(
              "~/Scripts/plugin/jquery-file-upload/js/vendor/jquery.ui.widget.js",
              "~/Scripts/plugin/jquery-file-upload/js/vendor/tmpl.min.js",
              "~/Scripts/plugin/jquery-file-upload/js/vendor/load-image.min.js",
              "~/Scripts/plugin/jquery-file-upload/js/vendor/canvas-to-blob.min.js",
              "~/Scripts/plugin/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js",
              "~/Scripts/plugin/jquery-file-upload/js/jquery.iframe-transport.js",
              "~/Scripts/plugin/jquery-file-upload/js/jquery.fileupload.js",
              "~/Scripts/plugin/jquery-file-upload/js/jquery.fileupload-process.js",
              "~/Scripts/plugin/jquery-file-upload/js/jquery.fileupload-image.js",
              "~/Scripts/plugin/jquery-file-upload/js/jquery.fileupload-audio.js",
              "~/Scripts/plugin/jquery-file-upload/js/jquery.fileupload-video.js",
              "~/Scripts/plugin/jquery-file-upload/js/jquery.fileupload-validate.js",
              "~/Scripts/plugin/jquery-file-upload/js/jquery.fileupload-ui.js"
            ));

            bundles.Add(new StyleBundle("~/Content/fileuploadCSS").Include(
               "~/Scripts/plugin/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css",
               "~/Scripts/plugin/jquery-file-upload/css/jquery.fileupload.css",
               "~/Scripts/plugin/jquery-file-upload/css/jquery.fileupload-ui.css"
            ));

            bundles.Add(new ScriptBundle("~/scripts/compass/datatables").Include(
                "~/scripts/DataTables/jquery.dataTables.min.js",
                "~/scripts/DataTables/dataTables.bootstrap.min.js"));

            BundleTable.EnableOptimizations = true;
        }
    }
}