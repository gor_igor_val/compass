﻿using Compass.EntityModels;
using Compass.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compass.Controllers
{
    public class ServiceCalendarController : Controller
    {
        CompassEntities db = new CompassEntities();

        // GET: ServiceCalendar
        [Authorize]
        public ActionResult Index()
        {
            var userGuid = User.Identity.GetUserId();
            var role = Convert.ToInt32(db.AspNetUsers.Find(userGuid).AspNetRoles.FirstOrDefault().CompaniesQuantity);
            switch (role)
            {
                case 0: //dispather
                    ViewBag.Companies = new SelectList(db.APP_COMPANY, "company_id", "name");
                    ViewBag.Objects = new SelectList(db.APP_ServiceObjects, "Id", "Name");
                    break;
                case 1: //user or validator
                    return HttpNotFound();
                case 2: // region admin
                    var myRegion = db.AspNetUsers.Find(userGuid).RegionId;
                    ViewBag.Companies = new SelectList(db.APP_COMPANY.Where(item => item.APP_ServiceObjects.Any(o => o.RegionId == myRegion)), "company_id", "name");
                    ViewBag.Objects = new SelectList(db.APP_ServiceObjects.Where(item => item.RegionId == myRegion), "Id", "Name");
                    break;
                default:
                    return HttpNotFound();
            }

            return View();
        }

        public JsonResult ChangeCompany(int? companyId)
        {
            try
            {
                var userGuid = User.Identity.GetUserId();
                var role = Convert.ToInt32(db.AspNetUsers.Find(userGuid).AspNetRoles.FirstOrDefault().CompaniesQuantity);
                List<ObjectsToSelect> objects = new List<ObjectsToSelect>();
                switch (role)
                {
                    case 0: //dispather
                        {
                            var temp = companyId == null ? db.APP_ServiceObjects : db.APP_ServiceObjects.Where(item => item.CompanyId == companyId);
                            objects = temp.Select(item => new ObjectsToSelect
                            {
                                Id = item.Id,
                                Name = item.Name
                            }).ToList();
                        }
                        break;
                    case 2: // region admin
                        {
                            var myRegion = db.AspNetUsers.Find(userGuid).RegionId;
                            var temp = companyId == null ? db.APP_ServiceObjects.Where(item => item.RegionId == myRegion) : db.APP_ServiceObjects.Where(item => item.CompanyId == companyId && item.RegionId == myRegion);
                            objects = temp.Select(item => new ObjectsToSelect
                            {
                                Id = item.Id,
                                Name = item.Name
                            }).ToList();
                        }
                        break;
                }

                return Json(new { Result = "OK", Objects = objects }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "Error", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ChangeObject(int objectId)
        {
            try
            {
                var equipments = db.APP_Equipment.Where(item => item.ObjectId == objectId).Select(item => new ObjectsToSelect
                {
                    Id = item.Id,
                    Name = item.Name
                }).ToList();

                return Json(new { Result = "OK", Objects = equipments }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "Error", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetEvents(int? companyId, int? objectId)
        {
            try
            {
                DateTime dateFrom = DateTime.Now.Date.AddDays(-7);
                List<CalendarTable> events = new List<CalendarTable>();
                List<APP_ServiceCalendar> temp = new List<APP_ServiceCalendar>();
                var userGuid = User.Identity.GetUserId();
                var role = Convert.ToInt32(db.AspNetUsers.Find(userGuid).AspNetRoles.FirstOrDefault().CompaniesQuantity);
                switch (role)
                {
                    case 0: //dispather
                        temp = db.APP_ServiceCalendar.Where(item => item.ServiceDate >= dateFrom).ToList();
                        break;
                    case 2: // region admin
                        var myRegion = db.AspNetUsers.Find(userGuid).RegionId;
                        temp = db.APP_ServiceCalendar.Where(item => item.ServiceDate >= dateFrom && item.APP_Equipment.APP_ServiceObjects.RegionId == myRegion).ToList();
                        break;
                }
                temp = companyId == null ? temp : temp.Where(item => item.APP_Equipment.APP_ServiceObjects.CompanyId == companyId).ToList();
                temp = objectId == null ? temp : temp.Where(item => item.APP_Equipment.ObjectId == objectId).ToList();
                events = temp.Select(item => new CalendarTable
                {
                    id = item.Id.ToString(),
                    start = item.ServiceDate,
                    title = item.APP_Equipment.Name,
                    className = item.CssClasses,
                    text = item.Text,
                    companyId = item.APP_Equipment.APP_ServiceObjects.CompanyId.ToString(),
                    objectId = item.APP_Equipment.ObjectId.ToString(),
                    equipmentId = item.EquipmentId.ToString(),
                    remember = item.Remember.ToString()
                }).ToList();

                return Json(new { Result = "OK", Events = events }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "Error", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult EventCreate(int EquipmentId, string Text, DateTime ServiceDate, bool Remember, string CssClasses)
        {
            try
            {
                var userGuid = User.Identity.GetUserId();
                int? id = db.ServiceCalendarCreate(EquipmentId, Text, ServiceDate, Remember, userGuid, DateTime.Now, CssClasses).FirstOrDefault();
                if (id == null)
                    return Json(new { Result = "Error", Message = "Error while adding record" }, JsonRequestBehavior.AllowGet);

                return Json(new { Result = "OK", Id = id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "Error", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult EventEdit(int Id, int EquipmentId, string Text, DateTime ServiceDate, bool Remember, string CssClasses)
        {
            try
            {
                int? result = db.ServiceCalendarEdit(Id, EquipmentId, Text, ServiceDate, Remember, CssClasses).FirstOrDefault();
                if (result != 1)
                    return Json(new { Result = "Error", Message = "Error while editing record" }, JsonRequestBehavior.AllowGet);

                return Json(new { Result = "OK", Id = Id}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "Error", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult EventDelete(int Id)
        {
            try
            {
                db.ServiceCalendarDelete(Id);
                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "Error", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
