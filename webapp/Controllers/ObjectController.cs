﻿using Compass.EntityModels;
using Compass.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compass.Controllers
{
    public class ObjectController : Controller
    {
        CompassEntities db = new CompassEntities();

        // GET: Object
        [Authorize]
        public ActionResult Index()
        {
            return View(db.APP_ServiceObjects);
        }

        // GET: Object/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            APP_ServiceObjects srvObject = db.APP_ServiceObjects.Find(id);
            if (srvObject != null)
            {
                ObjectDetailsViewModel model = new ObjectDetailsViewModel()
                {
                    Id = srvObject.Id,
                    Name = srvObject.Name,
                    Company = srvObject.APP_COMPANY.name,
                    Region = srvObject.DICT_REGION.Name,
                    Oblast = srvObject.APP_ObjectAddresses.Region,
                    District = srvObject.APP_ObjectAddresses.District,
                    City = srvObject.APP_ObjectAddresses.City,
                    Street = srvObject.APP_ObjectAddresses.Street,
                    House = srvObject.APP_ObjectAddresses.House,
                    ContactPerson = srvObject.ContactPerson,
                    PhoneNumber = srvObject.PhoneNumber,
                    Email = srvObject.Email
                };
                return View(model);
            }
            return HttpNotFound();
        }

        // GET: Object/Create
        [Authorize]
        public ActionResult Create()
        {
            SelectList companies = new SelectList(db.APP_COMPANY, "company_id", "name");
            ViewBag.Companies = companies;
            SelectList regions = new SelectList(db.DICT_REGION, "Id", "Name");
            ViewBag.Regions = regions;

            return View();
        }
        [HttpPost]
        public ActionResult Create(ObjectCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                db.AddServiceObject(model.Name, model.Company, model.Region, model.Oblast, model.District, model.City,
                    model.Street, model.House, model.ContactPerson, model.PhoneNumber, model.Email);
                return RedirectToAction("Index", "Object");
            }

            SelectList companies = new SelectList(db.APP_COMPANY, "company_id", "name");
            ViewBag.Companies = companies;
            SelectList regions = new SelectList(db.DICT_REGION, "Id", "Name");
            ViewBag.Regions = regions;

            return View(model);
        }

        // GET: Object/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            APP_ServiceObjects srvObject = db.APP_ServiceObjects.Find(id);
            if (srvObject != null)
            {
                SelectList companies = new SelectList(db.APP_COMPANY, "company_id", "name");
                ViewBag.Companies = companies;
                SelectList regions = new SelectList(db.DICT_REGION, "Id", "Name");
                ViewBag.Regions = regions;

                ObjectCreateViewModel model = new ObjectCreateViewModel
                {
                    Id = srvObject.Id,
                    Name = srvObject.Name,
                    Company = srvObject.CompanyId,
                    Region = srvObject.RegionId,
                    Oblast = srvObject.APP_ObjectAddresses.Region,
                    District = srvObject.APP_ObjectAddresses.District,
                    City = srvObject.APP_ObjectAddresses.City,
                    Street = srvObject.APP_ObjectAddresses.Street,
                    House = srvObject.APP_ObjectAddresses.House,
                    ContactPerson = srvObject.ContactPerson,
                    PhoneNumber = srvObject.PhoneNumber,
                    Email = srvObject.Email
                };
                return View(model);
            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Edit(ObjectCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                db.EditServiceObject(model.Id, model.Name, model.Company, model.Region, model.Oblast, model.District,
                    model.City, model.Street, model.House, model.ContactPerson, model.PhoneNumber, model.Email);
                return RedirectToAction("Index", "Object");

            }

            SelectList companies = new SelectList(db.APP_COMPANY, "company_id", "name");
            ViewBag.Companies = companies;
            SelectList regions = new SelectList(db.DICT_REGION, "Id", "Name");
            ViewBag.Regions = regions;
            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public string DeleteConfirmed(int id)
        {
            if (db.DeleteServiceObject(id) == -2)
            {
                return "DeleteError";
            }
            if (db.DeleteServiceObject(id) == -1)
            {
                return "DeleteErrorEquipment";
            }
            return "DeleteSuccess";          
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
