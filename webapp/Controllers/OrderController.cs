﻿using Compass.EntityModels;
using Compass.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Hosting;
using Compass.Helpers;

namespace Compass.Controllers
{
    public class OrderController : Controller
    {
        CompassEntities db = new CompassEntities();

        private bool CheckAccessOrder(int? orderId, string userGuid)
        {
            var role = Convert.ToInt32(db.AspNetUsers.Find(userGuid).AspNetRoles.FirstOrDefault().CompaniesQuantity);
            switch(role)
            {
                case 0: //dispatcher
                        return true;
                case 1: //initiator or validator
                    var objId = db.APP_Order.Find(orderId).ServiceObjectId;
                    return db.APP_ServiceObjects.Where(item => item.Id == objId).Any(item => item.AspNetUsers.Any(u => u.Id == userGuid));
                case 2: //region admin
                    var myRegion = db.AspNetUsers.Find(userGuid).RegionId;
                    return db.APP_Order.Any(item => item.Id == orderId && item.APP_ServiceObjects.RegionId == myRegion);
            }
            return false;
        }

        // ************************************************************************Головна таблиця заявок GET
        // GET: Order
        [Authorize]
        public ActionResult Index(int? id)
        {
            var userGuid = User.Identity.GetUserId();
            var role = Convert.ToInt32(db.AspNetUsers.Find(userGuid).AspNetRoles.FirstOrDefault().CompaniesQuantity);
            var records = db.APP_Order.AsEnumerable();
            switch(role)
            {
                case 1: //user or validator
                    var objects = db.APP_ServiceObjects.Where(item => item.AspNetUsers.Any(u => u.Id == userGuid)).Select(item => item.Id).ToList();
                    records = records.Where(item => objects.Contains(item.ServiceObjectId));
                    break;
                case 2: // region admin
                    var myRegion = db.AspNetUsers.Find(userGuid).RegionId;
                    records = records.Where(item => item.APP_ServiceObjects.RegionId == myRegion);
                    //var companies = db.APP_COMPANY.Where(item => item.AspNetUsers.Any(u => u.Id == userGuid)).Select(item => item.company_id).ToList();
                    //records = records.Where(item => companies.Contains(item.APP_ServiceObjects.CompanyId));
                    break;
                default:
                    if (id != null)
                    {
                        records = records.Where(item => item.APP_ServiceObjects.CompanyId == id);
                    }
                    break;
            }

            SelectList status = new SelectList(db.DICT_OrderStatus, "StatusIndex", "Name");
            ViewBag.OrderStatusList = status;
            SelectList type = new SelectList(db.DICT_OrderType, "Id", "Name");
            ViewBag.OrderTypeList = type;

            return View(records.ToList());
        }

        // GET: Order/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var userGuid = User.Identity.GetUserId();
            if (!CheckAccessOrder(id, userGuid))
            {
                return HttpNotFound();
            }


            APP_Order order = db.APP_Order.Find(id);
            if (order != null)
            {
                OrderDetailsViewModel model = new OrderDetailsViewModel()
                {
                    Id = order.Id,
                    Company = order.APP_ServiceObjects.APP_COMPANY.name,
                    Object = order.APP_ServiceObjects.Name,
                    Client = order.AspNetUsers1.FirstName + " " + order.AspNetUsers1.LastName,
                    Administrator = order.AspNetUsers.FirstName + " " + order.AspNetUsers.LastName,
                    Name = order.Name,
                    Content = order.Content,
                    OrderType = order.DICT_OrderType.Name,
                    Equipment = order.APP_Equipment?.Name,
                    OrderStatus = order.DICT_OrderStatus.Name,
                    Escalation = order.Escalation,
                    Process = order.Process,
                    Quality = order.Quality,
                    DocumentLink = order.DocumentLink,
                    Performer = order.Performer
                };

                List<CalendarTable> _events = new List<CalendarTable>();
                var records = db.APP_OrderLife.Where(item => item.OrderId == id).OrderBy(item => item.DateCreate).ToList();
                for (var i = 0; i < records.Count(); i++ )
                {
                    if (!(records[i].DICT_OrderStatus.StatusIndex == 6 || records[i].DICT_OrderStatus.StatusIndex == 8))
                    {
                        CalendarTable _event = new CalendarTable();
                        _event.start = records[i].DateCreate;
                        if (i == records.Count()-1)
                        {
                            _event.end = DateTime.Now;
                        }
                        else
                        {
                            _event.end = records[i+1].DateCreate;
                        }
                        _event.title = records[i].DICT_OrderStatus.Name;
                        _event.id = records[i].Id.ToString();
                        _event.className = records[i].DICT_OrderStatus.ClassesToCalendar == null ? _event.className : records[i].DICT_OrderStatus.ClassesToCalendar;
                        _events.Add(_event);
                    }
                }
                ViewBag.Events = _events.AsEnumerable();

                return View(model);
            }
            return HttpNotFound();
        }

        // GET: Order/Create
        [Authorize]
        public ActionResult Create()
        {
            var userGuid = User.Identity.GetUserId();
            var role = Convert.ToInt32(db.AspNetUsers.Find(userGuid).AspNetRoles.FirstOrDefault().CompaniesQuantity);

            SelectList companies = new SelectList(role == 0 ? db.APP_COMPANY : db.APP_COMPANY.Where(c => c.AspNetUsers.Any(u => u.Id == userGuid)), "company_id", "name");
            ViewBag.CompaniesList = companies;

            List<UsersToSelect> currentUser = db.AspNetUsers.Where(item => item.Id == userGuid).Select(item => new UsersToSelect {
                Id = item.Id,
                FullName = item.FirstName + " " + item.LastName
            }).ToList();
            SelectList users = new SelectList(currentUser, "Id", "FullName");
            ViewBag.ClientsList = users;

            SelectList types = new SelectList(db.DICT_OrderType, "Id", "Name");
            ViewBag.OrderTypeList = types;

            SelectList status = new SelectList(db.DICT_OrderStatus, "StatusIndex", "Name");
            ViewBag.OrderStatusList = status;

            ViewBag.Role = role;

            ViewBag.DocLink = "/OrderDocs/" + Guid.NewGuid().ToString()+"/";

            return View();
        }

        static String tempPath = "~/tempfiles/";
        static String DeleteType = "GET";

        [HttpPost]
        public JsonResult Upload(string path)
        {

            string serverMapPath = "~" + path;
            string StorageRoot = Path.Combine(HostingEnvironment.MapPath(serverMapPath));
            string UrlBase = path;
            string DeleteURL = "/Order/DeleteFile/?path=" + path + "&file=";
            FilesHelper filesHelper = new FilesHelper(DeleteURL, DeleteType, StorageRoot, UrlBase, tempPath, serverMapPath);

            var resultList = new List<ViewDataUploadFilesResult>();

            var CurrentContext = HttpContext;

            filesHelper.UploadAndShowResults(CurrentContext, resultList);
            JsonFiles files = new JsonFiles(resultList);

            bool isEmpty = !resultList.Any();
            if (isEmpty)
            {
                return Json("Error ");
            }
            else
            {
                return Json(files);
            }
        }

        public JsonResult GetFileList(string path)
        {
            string serverMapPath = "~" + path;
            string StorageRoot = Path.Combine(HostingEnvironment.MapPath(serverMapPath));
            string UrlBase = path;
            string DeleteURL = "/Order/DeleteFile/?path=" + path + "&file=";
            FilesHelper filesHelper = new FilesHelper(DeleteURL, DeleteType, StorageRoot, UrlBase, tempPath, serverMapPath);

            var list = filesHelper.GetFileList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteFile(string path, string file)
        {
            string serverMapPath = "~" + path;
            string StorageRoot = Path.Combine(HostingEnvironment.MapPath(serverMapPath));
            string UrlBase = path;
            string DeleteURL = "/Order/DeleteFile/?path=" + path + "&file=";
            FilesHelper filesHelper = new FilesHelper(DeleteURL, DeleteType, StorageRoot, UrlBase, tempPath, serverMapPath);

            filesHelper.DeleteFile(file);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(OrderCreateViewModel model)
        {
            var userGuid = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                db.AddOrder(userGuid, DateTime.Now, model.Object, model.Client, model.Administrator, model.Name, model.Content, 
                    model.OrderType, model.Equipment, db.DICT_OrderStatus.First(item => item.StatusIndex == 1).Id, model.Escalation, model.Process, model.Quality, model.DocumentLink, model.Performer);

                return RedirectToAction("Index", "Order");
            }

            var role = Convert.ToInt32(db.AspNetUsers.Find(userGuid).AspNetRoles.FirstOrDefault().CompaniesQuantity);

            SelectList companies = new SelectList(role == 0 ? db.APP_COMPANY : db.APP_COMPANY.Where(c => c.AspNetUsers.Any(u => u.Id == userGuid)), "company_id", "name");
            ViewBag.CompaniesList = companies;

            List<UsersToSelect> currentUser = db.AspNetUsers.Where(item => item.Id == userGuid).Select(item => new UsersToSelect
            {
                Id = item.Id,
                FullName = item.FirstName + " " + item.LastName
            }).ToList();
            SelectList users = new SelectList(currentUser, "Id", "FullName");
            try
            {
                List<UsersToSelect> relatedUsers = db.AspNetUsers.Where(u => u.APP_COMPANY.Any(c => c.company_id == model.Company)).Select(item => new UsersToSelect
                {
                    Id = item.Id,
                    FullName = item.FirstName + " " + item.LastName
                }).ToList();
                currentUser.AddRange(relatedUsers.Where(p2 => currentUser.All(p1 => p1.FullName != p2.FullName)));

                users = new SelectList(currentUser, "Id", "FullName");
            }
            catch (Exception e) { }
            ViewBag.ClientsList = users;

            SelectList types = new SelectList(db.DICT_OrderType, "Id", "Name");
            ViewBag.OrderTypeList = types;

            SelectList status = new SelectList(db.DICT_OrderStatus, "StatusIndex", "Name");
            ViewBag.OrderStatusList = status;

            ViewBag.Role = role;

            ViewBag.TempPath = model.DocumentLink;

            return View(model);
        }

        public JsonResult ChangeCompany(int companyId)
        {
            try
            {
                var userGuid = User.Identity.GetUserId();
                List<UsersToSelect> users = db.AspNetUsers.Where(item => item.Id == userGuid).Select(item => new UsersToSelect
                {
                    Id = item.Id,
                    FullName = item.FirstName + " " + item.LastName
                }).ToList();
                List<UsersToSelect> relatedUsers = db.AspNetUsers.Where(u => u.APP_COMPANY.Any(c => c.company_id == companyId)).Select(item => new UsersToSelect
                {
                    Id = item.Id,
                    FullName = item.FirstName + " " + item.LastName
                }).ToList();
                users.AddRange(relatedUsers.Where(p2 => users.All(p1 => p1.FullName != p2.FullName)));

                var objects = db.APP_ServiceObjects.Where(item => item.CompanyId == companyId).Select(item => new ObjectsToSelect
                {
                    Id = item.Id,
                    Name = item.Name
                }).ToList();

                return Json(new { Result = "OK", Users = users, Objects = objects }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "Error", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ChangeObject(int objectId)
        {
            try
            {
                var myRegion = db.APP_ServiceObjects.Find(objectId).RegionId;
                var admin = db.AspNetUsers.Where(item => item.RegionId == myRegion).Select(item => new UsersToSelect
                {
                    Id = item.Id,
                    FullName = item.FirstName + " " + item.LastName
                }).FirstOrDefault();

                var equipments = db.APP_Equipment.Where(item => item.ObjectId == objectId).Select(item => new ObjectsToSelect
                {
                    Id = item.Id,
                    Name = item.Name
                }).ToList();

                return Json(new { Result = "OK", Admin = admin, Equipments = equipments }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "Error", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Order/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var userGuid = User.Identity.GetUserId();
            if (!CheckAccessOrder(id, userGuid))
            {
                return HttpNotFound();
            }


            APP_Order order = db.APP_Order.Find(id);
            if (order != null)
            {
                OrderCreateViewModel model = new OrderCreateViewModel
                {
                    Id = order.Id,
                    Company = order.APP_ServiceObjects.CompanyId,
                    Object = order.ServiceObjectId,
                    Client = order.ClientId,
                    Administrator = order.AdministratorId,
                    Name = order.Name,
                    Content = order.Content,
                    OrderType = order.OrderTypeId,
                    Equipment = order.EquipmentId,
                    OrderStatus = order.OrderStatusId,
                    Escalation = order.Escalation,
                    Process = order.Process,
                    Quality = order.Quality,
                    DocumentLink = order.DocumentLink,
                    Performer = order.Performer
                };

                var role = Convert.ToInt32(db.AspNetUsers.Find(userGuid).AspNetRoles.FirstOrDefault().CompaniesQuantity);

                SelectList companies = new SelectList(role == 0 ? db.APP_COMPANY : db.APP_COMPANY.Where(c => c.AspNetUsers.Any(u => u.Id == userGuid)), "company_id", "name", model.Company);
                ViewBag.CompaniesList = companies;

                SelectList objects = new SelectList(db.APP_ServiceObjects.Where(item => item.CompanyId == model.Company), "Id", "Name", model.Object);
                ViewBag.ObjectsList = objects;

                List<UsersToSelect> currentUser = db.AspNetUsers.Where(item => item.Id == userGuid).Select(item => new UsersToSelect
                {
                    Id = item.Id,
                    FullName = item.FirstName + " " + item.LastName
                }).ToList();
                try
                {
                    List<UsersToSelect> relatedUsers = db.AspNetUsers.Where(u => u.APP_COMPANY.Any(c => c.company_id == model.Company)).Select(item => new UsersToSelect
                    {
                        Id = item.Id,
                        FullName = item.FirstName + " " + item.LastName
                    }).ToList();
                    currentUser.AddRange(relatedUsers.Where(p2 => currentUser.All(p1 => p1.FullName != p2.FullName)));

                }
                catch (Exception e) { }
                var users = new SelectList(currentUser, "Id", "FullName", model.Client);
                ViewBag.ClientsList = users;

                var myRegion = db.APP_ServiceObjects.Find(model.Object).RegionId;
                var admin = db.AspNetUsers.Where(item => item.RegionId == myRegion).Select(item => new UsersToSelect
                {
                    Id = item.Id,
                    FullName = item.FirstName + " " + item.LastName
                }).FirstOrDefault();
                SelectListItem selListItem = new SelectListItem() { Value = admin.Id, Text = admin.FullName };
                List<SelectListItem> newList = new List<SelectListItem>();
                newList.Add(selListItem);
                SelectList adminList = new SelectList(newList, "Value", "Text", model.Administrator);
                ViewBag.AdminList = adminList;

                SelectList types = new SelectList(db.DICT_OrderType, "Id", "Name", model.OrderType);
                ViewBag.OrderTypeList = types;

                SelectList equipments = new SelectList(db.APP_Equipment.Where(item => item.ObjectId == model.Object), "Id", "Name", model.Equipment);
                ViewBag.EquipmentList = equipments;

                SelectList status = new SelectList(db.GetOrderStatus(userGuid, model.Id), "StatusIndex", "Name", model.OrderStatus);
                ViewBag.OrderStatusList = status;

                ViewBag.Role = role;

                return View(model);
            }
            return HttpNotFound();
        }

        // POST: Order/Edit/5
        [HttpPost]
        public ActionResult Edit(OrderCreateViewModel model)
        {
            var userGuid = User.Identity.GetUserId();
            var role = Convert.ToInt32(db.AspNetUsers.Find(userGuid).AspNetRoles.FirstOrDefault().CompaniesQuantity);
            if (!CheckAccessOrder(model.Id, userGuid))
            {
                ModelState.AddModelError("", "Access denied");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    APP_Order old_order = db.APP_Order.Find(model.Id);
                    if (!(role == 0 || model.OrderStatus == 1)) // если не диспетчер или заявка не новая, то следующие поля менять запрещено
                    {
                        model.Company = old_order.APP_ServiceObjects.CompanyId;
                        model.Object = old_order.ServiceObjectId;
                        model.Client = old_order.ClientId;
                        model.Administrator = old_order.AdministratorId;
                        model.Name = old_order.Name;
                        model.Content = old_order.Content;
                        model.OrderType = old_order.OrderTypeId;
                        model.Equipment = old_order.EquipmentId;
                    }
                    if (!(role == 0 || (model.OrderStatus <= 3 && role == 2)))
                    {
                        model.Performer = old_order.Performer;
                    }
                    if (role != 1) //оценку качества может ставить только юзер
                    {
                        model.Quality = old_order.Quality;
                    }
                    var result = db.EditOrder(userGuid, DateTime.Now, model.Id, model.Object, model.Client, model.Administrator, model.Name, model.Content,
                    model.OrderType, model.Equipment, db.DICT_OrderStatus.First(item => item.StatusIndex == model.OrderStatus).Id, model.Escalation, model.Process, model.Quality, model.DocumentLink, model.Performer).FirstOrDefault();

                    if (result == -1)
                    {
                        ModelState.AddModelError("", Resources.MainResource.Order_NotExistError);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Order");
                    }
                }
            }

            SelectList companies = new SelectList(role == 0 ? db.APP_COMPANY : db.APP_COMPANY.Where(c => c.AspNetUsers.Any(u => u.Id == userGuid)), "company_id", "name", model.Company);
            ViewBag.CompaniesList = companies;

            SelectList objects = new SelectList(db.APP_ServiceObjects.Where(item => item.CompanyId == model.Company), "Id", "Name", model.Object);
            ViewBag.ObjectsList = objects;

            List<UsersToSelect> currentUser = db.AspNetUsers.Where(item => item.Id == userGuid).Select(item => new UsersToSelect
            {
                Id = item.Id,
                FullName = item.FirstName + " " + item.LastName
            }).ToList();
            try
            {
                List<UsersToSelect> relatedUsers = db.AspNetUsers.Where(u => u.APP_COMPANY.Any(c => c.company_id == model.Company)).Select(item => new UsersToSelect
                {
                    Id = item.Id,
                    FullName = item.FirstName + " " + item.LastName
                }).ToList();
                currentUser.AddRange(relatedUsers.Where(p2 => currentUser.All(p1 => p1.FullName != p2.FullName)));

            }
            catch (Exception e) { }
            var users = new SelectList(currentUser, "Id", "FullName", model.Client);
            ViewBag.ClientsList = users;

            var myRegion = db.APP_ServiceObjects.Find(model.Object).RegionId;
            var admin = db.AspNetUsers.Where(item => item.RegionId == myRegion).Select(item => new UsersToSelect
            {
                Id = item.Id,
                FullName = item.FirstName + " " + item.LastName
            }).FirstOrDefault();
            SelectListItem selListItem = new SelectListItem() { Value = admin.Id, Text = admin.FullName };
            List<SelectListItem> newList = new List<SelectListItem>();
            newList.Add(selListItem);
            SelectList adminList = new SelectList(newList, "Value", "Text", model.Administrator);
            ViewBag.AdminList = adminList;

            SelectList types = new SelectList(db.DICT_OrderType, "Id", "Name", model.OrderType);
            ViewBag.OrderTypeList = types;

            SelectList equipments = new SelectList(db.APP_Equipment.Where(item => item.ObjectId == model.Object), "Id", "Name", model.Equipment);
            ViewBag.EquipmentList = equipments;

            SelectList status = new SelectList(db.GetOrderStatus(userGuid, model.Id), "StatusIndex", "Name", model.OrderStatus);
            ViewBag.OrderStatusList = status;

            ViewBag.Role = role;

            return View(model);
        }

        [HttpPost]
        public JsonResult PerformerAutocomplete(string Prefix)
        {
            List<string> list = new List<string>();
            list = db.APP_Order.Where(item => item.Performer != null && item.Performer.StartsWith(Prefix)).Select(item => item.Performer).Distinct().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        // GET: Order/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Order/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
