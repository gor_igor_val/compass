﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Compass.EntityModels;
using Compass.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using Compass.Utils;
using Compass;

namespace SmartAdminMvc.Controllers
{
    public class UsersController : Controller
    {
        CompassEntities db = new CompassEntities();

        private ApplicationUserManager UserManager
            => HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();


        // GET: Users
        [Authorize]
        public ActionResult Index(int? id)
        {

            if (id == null)
            {
                return View(db.AspNetUsers);
            }
            else
            {
                return View(db.AspNetUsers.Where(item => item.APP_COMPANY.FirstOrDefault().company_id == id));
            }
        }

        // GET: Users/Details/5
        [Authorize]
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            ApplicationUser user = await UserManager.FindByIdAsync(id);//получаем данные выбраного пользователя

            if (user == null)
            {
                return HttpNotFound();
            }

            //string company = "";
            ////если процедура возвращает компанию записывает ее в переменную которая присваивается полю Компании, если нет поле пустое
            //if (db.GetUserCompany(user.Id).FirstOrDefault() != null)
            //{
            //     company += db.GetUserCompany(user.Id).FirstOrDefault();
            //}
            //else company = "";

            //поля формы будут равны данным пользователя
            var model = new DetailsViewModel
            {
                UserName = user.UserName,
                FirstName = user.FirstName ?? "",
                LastName = user.LastName ?? "",
                Patronymic = user.Patronymic ?? "",
                Phone = user.PhoneNumber ?? "",
                Email = user.Email,
                Role = db.GetUserRole(user.Id).FirstOrDefault() ?? "",
                Company = string.Join("; ", db.GetUserCompany(user.Id).ToArray()),
                ServiceObject = string.Join("; ", db.GetUserObject(user.Id).ToArray())

            };
            ViewBag.Id = id;
            return View(model);
        }

        // GET: Users/Create
        [Authorize]
        public ActionResult Create()
        {
            // DropDownList заполняется значениями из таблицы AspNetRoles
            ViewBag.roles = new SelectList(db.AspNetRoles.AsEnumerable(), "Id", "Name", 0);
            // DropDownList заполняется значениями из таблицы APP_COMPANY
            ViewBag.companies = new SelectList(db.APP_COMPANY.AsEnumerable(), "company_id", "name", 0);
            // DropDownList заполняется значениями из таблицы APP_ServiceObjects
            ViewBag.serviceObjects = new SelectList(db.APP_ServiceObjects.AsEnumerable(), "Id", "Name", 0);

            ViewBag.Regions = new SelectList(db.DICT_REGION, "Id", "Name");

            return View();
        }

        // POST: Users/Create
        [HttpPost]
        public async Task<ActionResult> Create(CreateViewModel model)
        {
            //Ручная валидация для случая если региональный управляющий для заданого региона уже существует
            //Если роль - региональный управляющий и региональный управляющий для заданого региона уже существует, ошибка валидации
            //Иначе если другая роль или регионального управляющего можна создать, то проблем нет
            bool checkIsManager = !CheckIsManager(model.Role) ||
                                  !ManagerIsExists(model.RegionId);

            if (ModelState.IsValid && checkIsManager)
            {
                var user = new ApplicationUser { UserName = model.UserName, FirstName = model.FirstName, LastName = model.LastName, Patronymic = model.Patronymic, Email = model.Email, PhoneNumber = model.Phone };

                var result = await UserManager.CreateAsync(user, model.Password);//создает пользователя с заповненимим полями в users
                if (result.Succeeded)
                {
                    EmailClient emailClient = new EmailClient();

                    emailClient.SendMessage(model.Email, Resources.MainResource.Users_NewUser,
                        Request.Url.Host + "\n" + Resources.MainResource.Users_Email_Login
                        + ": " + model.UserName + " \n" + Resources.MainResource.Shared_Password + ": " +
                        model.Password);

                    db.SetUserRole(user.Id, model.Role);//добавляется роль пользователя

                    //если выбраная роль пользователя это "региональный управляющий", тогда в базу данных 
                    //добавляется список компаний
                    //И ид региона - прим.
                    if (db.GetCompaniesQuantity(model.Role).First() == "2")
                    {
                        //Добавить регион для регионального менеджера (проверка валидности выше)
                        var u = db.AspNetUsers.Find(user.Id);
                        u.RegionId = model.RegionId;
                        db.Entry(u).State = EntityState.Modified;
                        db.SaveChanges();//

                        if (model.Company != null)
                        {
                            foreach (var item in model.Company)
                            {
                                db.SetUserComp(user.Id, item);
                            }
                        }
                    }
                    else if (db.GetCompaniesQuantity(model.Role).First() == "1")
                    {
                        if (model.Company != null)
                        {
                            db.SetUserComp(user.Id, model.Company.First());
                        }
                    }

                    //если выбраная роль пользователя это "Ініціатор", тогда в базу данных добавляется список обьектов
                    if (db.GetObjectQuantity(model.Role).First() == "2")
                    {
                        if (model.ServiceObject != null)
                        {
                            foreach (var item in model.ServiceObject)
                            {
                                db.SetUserObject(user.Id, item);
                            }
                        }
                    }
                    else if (db.GetCompaniesQuantity(model.Role).First() == "1")
                    {
                        if (model.ServiceObject != null)
                        {
                            db.SetUserObject(user.Id, model.ServiceObject.FirstOrDefault());
                        }
                    }

                    return RedirectToAction("Index", "Users");
                }
                AddErrors(result);
            }

            //если форма не прошла валидацию, снова заполнить DropDownList
            ViewBag.roles = new SelectList(db.AspNetRoles.AsEnumerable(), "Id", "Name", 0);
            ViewBag.companies = new SelectList(db.APP_COMPANY.AsEnumerable(), "company_id", "name", 0);
            ViewBag.serviceObjects = new SelectList(db.APP_ServiceObjects.AsEnumerable(), "Id", "Name", 0);

            ViewBag.Regions = new SelectList(db.DICT_REGION, "Id", "Name");

            return View();
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        // GET: Users/Edit/5
        [Authorize]
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            //получаем данные от выбраного пользователя
            ApplicationUser user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            // DropDownList заполняется значениями из таблицы AspNetRoles
            ViewBag.roles = new SelectList(db.AspNetRoles.AsEnumerable(), "Id", "Name");
            // DropDownList заполняется значениями из таблицы APP_COMPANY
            ViewBag.companies = new SelectList(db.APP_COMPANY.AsEnumerable(), "company_id", "name", 0);
            // DropDownList заполняется значениями из таблицы APP_ServiceObjects
            ViewBag.serviceObjects = new SelectList(db.APP_ServiceObjects.AsEnumerable(), "Id", "Name", 0);

            ViewBag.Regions = new SelectList(db.DICT_REGION, "Id", "Name");

            //поля формы будут равны данным пользователя
            var model = new EditViewModel
            {
                UserName = user.UserName,
                FirstName = user.FirstName ?? "",
                LastName = user.LastName ?? "",
                Patronymic = user.Patronymic ?? "",
                Phone = user.PhoneNumber ?? "",
                Email = user.Email,
                Password = "",
                PasswordConfirm = "",
                //Role = db.GetUserRole(user.Id).FirstOrDefault() ?? "", //Возвращает не Ид роли, а название, WTF?!
                Role = user.Roles.FirstOrDefault().RoleId,//-new
                Company = db.GetUserCompany(user.Id).ToList(),
                RegionId = db.AspNetUsers.Find(id).RegionId//-new
            };
            return View(model);
        }

        // POST: Users/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(EditViewModel model)
        {
            //Ручная валидация для случая если региональный управляющий для заданого региона уже существует
            //Если роль - региональный управляющий и региональный управляющий для заданого региона уже существует, ошибка валидации
            //Иначе если другая роль или регионального управляющего можна создать, то проблем нет
            bool checkIsManager = !CheckIsManager(model.Role) ||
                                  !ManagerIsExists(model.RegionId, model.Id);

            if (ModelState.IsValid && checkIsManager)
            {
                ApplicationUser user = await UserManager.FindByIdAsync(model.Id);
                if (user != null)
                {
                    //
                    if (CheckIsManager(model.Role))
                    {
                        //Изменить регион для регионального менеджера (проверка валидности выше)
                        var u = db.AspNetUsers.Find(user.Id);
                        u.RegionId = model.RegionId;
                        db.Entry(u).State = EntityState.Modified;
                        db.SaveChanges();//
                    }
                    else
                    {
                        var u = db.AspNetUsers.Find(user.Id);
                        u.RegionId = null;
                        db.Entry(u).State = EntityState.Modified;
                        db.SaveChanges();
                    }//

                    user.UserName = model.UserName;
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.Patronymic = model.Patronymic;
                    user.PhoneNumber = model.Phone;
                    user.Email = model.Email;

                    if (!string.IsNullOrEmpty(model.Password))//если поле "Пароль" не пустое
                    {
                        var res = UserManager.RemovePassword(user.Id);//стереть пароль у пользователя с таким id
                        if (!res.Succeeded)
                        {
                            ModelState.AddModelError("", Resources.MainResource.Error_OccuredError);
                        }
                        else
                        {
                            res = UserManager.AddPassword(user.Id, model.Password);//добавить пароль для пользователя с таким id
                            EmailClient emailClient = new EmailClient();
                            emailClient.SendMessage(model.Email, Resources.MainResource.Users_Email_PasswordChanged,
                                "http://compass-portal.azurewebsites.net\n" + Resources.MainResource.Users_Email_Login
                                + ": " + model.UserName + " \n" + Resources.MainResource.Shared_Password + ": " +
                                model.Password);
                            if (!res.Succeeded)
                            {
                                ModelState.AddModelError("", Resources.MainResource.Error_OccuredError);
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(model.Role))//Если роль пользователя была изменена, меняет ее на установленную
                    {
                        db.EditUserRole(user.Id, model.Role);
                    }
                    //Если компании пользователя были изменены, удаляет предыдущие и меняет их на установленные
                    if (model.Company != null)
                    {
                        db.DeleteUserCompany(user.Id);
                        foreach (var item in model.Company)
                        {
                            db.SetUserComp(user.Id, item.ToString());
                        }
                    }

                    //Если была изменена роль на Диспетчер удаляет предустановленные компании
                    if (!string.IsNullOrEmpty(model.Role))
                    {
                        if (((model.Company) == null) && (db.GetCompaniesQuantity(model.Role).First() == "0"))
                        {
                            db.DeleteUserCompany(user.Id);
                        }
                    }

                    //Если обьекты пользователя были изменены, удаляет предыдущие и меняет их на установленные
                    if (model.ServiceObject != null)
                    {
                        db.DeleteUserObject(user.Id);
                        foreach (var item in model.ServiceObject)
                        {
                            db.SetUserObject(user.Id, item.ToString());
                        }
                    }

                    //Если была изменена роль на Диспетчер или на Региональный управляющий удаляет предустановленные обьекты
                    if (!string.IsNullOrEmpty(model.Role))
                    {
                        if ((model.ServiceObject == null) && (db.GetObjectQuantity(model.Role).First() == "0"))
                        {
                            db.DeleteUserObject(user.Id);
                        }
                    }

                    //обновить данные пользователя
                    IdentityResult result = await UserManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index", "Users");
                    }
                    AddErrors(result);

                    //ModelState.AddModelError("", Resources.MainResource.Error_OccuredError);
                }
                else
                {
                    ModelState.AddModelError("", Resources.MainResource.Users_Edit_LogoutError);
                }
            }
            //если форма не прошла валидацию, снова заполнить DropDownList
            ViewBag.roles = new SelectList(db.AspNetRoles.AsEnumerable(), "Id", "Name", 0);
            ViewBag.companies = new SelectList(db.APP_COMPANY.AsEnumerable(), "company_id", "name", 0);
            // DropDownList заполняется значениями из таблицы APP_ServiceObjects
            ViewBag.serviceObjects = new SelectList(db.APP_ServiceObjects.AsEnumerable(), "Id", "Name", 0);

            ViewBag.Regions = new SelectList(db.DICT_REGION, "Id", "Name");



            return View(model);

        }

        // POST: Users/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        public async Task<string> DeleteConfirmed(string id)
        {
            ApplicationUser user = await UserManager.FindByIdAsync(id);
            if (user != null)
            {
                db.DeleteUser(id);
                string login = user.UserName;
                IdentityResult result = await UserManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    EmailClient emailClient = new EmailClient();
                    emailClient.SendMessage(user.Email, Resources.MainResource.Users_Email_UserDeleted,
                        login + "\n" + Resources.MainResource.Users_Email_UserDeletedText + "\n" +
                        Request.Url.Host);

                    return "DeleteSuccess";
                }
            }
            return "DeleteError";
        }

        //Метод для запроса проверки существования менеджера региона из клиентского кода(JqueryValidate/Remote)
        [HttpGet]
        public JsonResult CheckManagerIsExists(int regionId)
        {
            //var manager = db.AspNetUsers.FirstOrDefault(user => user.RegionId == regionId);
            //var result = manager == null;
            bool result = !ManagerIsExists(regionId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //Внутренний метод для проверки существования менеджера у региона
        //Если у даного региона уже назначен менеджер, вернуть true
        private bool ManagerIsExists(int? regionId)
        {
            if (regionId == null)
            {
                return true;
            }

            //Здесь надо еще добавить логику валидации если при отключенной клиентской 
            //валидации придет значение региона, которого нет в списке регионов

            var manager = db.AspNetUsers.FirstOrDefault(user => user.RegionId == regionId);
            return manager != null;
        }

        private bool ManagerIsExists(int? regionId, string managerId)
        {
            if (regionId == null)
            {
                return true;
            }

            //Здесь надо еще добавить логику валидации если при отключенной клиентской 
            //валидации придет значение региона, которого нет в списке регионов

            var manager = db.AspNetUsers.Find(managerId);
            if (manager.RegionId == regionId)
            {
                return false;
            }

            manager = db.AspNetUsers.FirstOrDefault(user => user.RegionId == regionId);
            return manager != null;
        }

        private bool CheckIsManager(string roleId)
        {
            if (db.GetCompaniesQuantity(roleId).FirstOrDefault() == "2")
            {
                return true;
            }

            return false;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
