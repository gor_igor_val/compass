﻿using Compass.EntityModels;
using Compass.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compass.Controllers
{
    public class CompanyController : Controller
    {
        CompassEntities db = new CompassEntities();

        // ************************************************************************Головна таблиця організацій GET
        // GET: Company
        [Authorize]
        public ActionResult Index()
        {
            return View(db.APP_COMPANY);
        }

        // ************************************************************************Створення організації GET
        // GET: Company/Create
        [Authorize]
        public ActionResult Create()
        {
            CompanyCreateViewModel model = new CompanyCreateViewModel();
            model.QualityParams = new List<QualityParams>();
            List<DICT_QUALITY_PARAMS> qualityParams = db.DICT_QUALITY_PARAMS.ToList();
            foreach (var param in qualityParams)
            {
                model.QualityParams.Add(new QualityParams() { Id = param.param_id, Param = param.name });
            }
            return View(model);
        }

        // ************************************************************************Створення організації POST
        [HttpPost]
        public ActionResult Create(CompanyCreateViewModel model, HttpPostedFileBase upload)
        {
            Guid g = Guid.NewGuid();
            string folder = "~/Agreements/" + g.ToString();//папка с договорами
            int? result;
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength <= 15000000)//если файл выбран и он меньше или равен 15Мб
                {
                    // получаем имя файла
                    string fileName = System.IO.Path.GetFileName(upload.FileName);
                    string sendTo = folder + "/" + fileName;
                    Directory.CreateDirectory(Server.MapPath(folder));
                    upload.SaveAs(Server.MapPath(sendTo));
                    result = db.CreateCompany(model.Name, model.Adress, model.ContactPerson, model.Number, model.Email, sendTo, model.NeedValidation).FirstOrDefault();
                }
                else if (upload != null && upload.ContentLength > 15000000)//если файл выбран и он больше 15Мб
                {
                    ModelState.AddModelError("", Resources.MainResource.File_SizeLimit_Error);
                    result = -2;
                    //return Content("<script>alert('" + Resources.MainResource.File_SizeLimit_Error + "'); window.location.href = \"/Company/Create\";</script> ");

                }
                else
                {
                    ModelState.AddModelError("", Resources.TempResource.FileUploadError);
                    result = -2;
                    //result = db.CreateCompany(model.Name, model.Adress, model.ContactPerson, model.Number, model.Email, null, model.NeedValidation).FirstOrDefault();
                }

                if (result == 1) // 1 - Створення успішне
                {
                    try
                    {
                        // Получить id последней добавленной записи, чтобы использовать его для вызова процедуры AddQualityParamCompany
                        int? id = db.APP_COMPANY.OrderByDescending(p => p.company_id).FirstOrDefault().company_id;
                        // Добавляем параметры качества в бд
                        foreach (var qualityParam in model.QualityParams)
                        {
                            if (qualityParam.Value != null)
                                db.AddQualityParamCompany(id, qualityParam.Id, qualityParam.Value);
                        }
                    }
                    catch (Exception ex) { }

                    return RedirectToAction("Index", "Company"); // Створення успішне
                }


                if (result == 0)
                {
                    ModelState.AddModelError("", Resources.MainResource.Company_Create_Error1);//Створення не виконалось (-1)
                }
                else if (result == -1)
                {
                    ModelState.AddModelError("", Resources.MainResource.Company_Create_Error2);//Створення не виконалось, компанія з такою назвою уже присутня (-1)
                }
            }

            try
            {
                Directory.Delete(Server.MapPath(folder), true);
            }
            catch { };
            return View(model);
        }

        // ************************************************************************Редагування організації GET
        // GET: Company/Edit/id
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            APP_COMPANY Company = db.APP_COMPANY.Find(id);
            if (Company != null)
            {
                CompanyCreateViewModel model = new CompanyCreateViewModel
                {
                    Id = Company.company_id.ToString(),
                    Name = Company.name,
                    Adress = Company.adress,
                    Number = Company.number,
                    ContactPerson = Company.contact_person,
                    Email = Company.email,
                    LinkToFileAgreement = Company.link_to_file_agreement,
                    NeedValidation = Company.need_validation
                };

                model.QualityParams = new List<QualityParams>();
                List<DICT_QUALITY_PARAMS> qualityParams = db.DICT_QUALITY_PARAMS.ToList();

                foreach (var param in qualityParams)
                {
                    var APP_COMPANY_QUALITY_PARAM = db.APP_COMPANY_QUALITY_PARAMS.Where(p => p.company_id == Company.company_id).Where(p => p.param_id == param.param_id).FirstOrDefault();
                    short? value = (APP_COMPANY_QUALITY_PARAM != null) ? APP_COMPANY_QUALITY_PARAM.value : null;
                    model.QualityParams.Add(new QualityParams() { Id = param.param_id, Param = param.name, Value = value });
                }

                return View(model);
            }
            return HttpNotFound();
        }

        // ************************************************************************Редагівання організації POST
        [HttpPost]
        public ActionResult Edit(CompanyCreateViewModel model, HttpPostedFileBase upload, int? id)
        {
            Guid g = Guid.NewGuid();
            string folder = "~/Agreements/" + g.ToString();//папка с договорами
            if (id == null)
            {
                return HttpNotFound();
            }
            APP_COMPANY Company = db.APP_COMPANY.Find(id);
            if (ModelState.IsValid)
            {
                string path = Company.link_to_file_agreement;
                if (upload != null && upload.ContentLength <= 15000000)//если файл выбран удаляет придедущий и устанавливает новий
                {
                    try
                    {
                        Directory.Delete(Server.MapPath(path.Replace(Path.GetFileName(path), "")), true);//пробуем удалять папку с файлом
                    }
                    catch (Exception e) { }
                    string fileName = System.IO.Path.GetFileName(upload.FileName);
                    path = folder + "/" + fileName;
                    Directory.CreateDirectory(Server.MapPath(folder));
                    upload.SaveAs(Server.MapPath(path));
                }
                else if (upload != null && upload.ContentLength > 15000000)//если файл выбран и он больше 15Мб
                {
                    ModelState.AddModelError("", Resources.MainResource.File_SizeLimit_Error);
                    return View(model);
                }
                var result = db.EditCompany(Convert.ToInt32(model.Id), model.Name, model.Adress, model.ContactPerson, model.Number, model.Email,
                    path, model.NeedValidation).FirstOrDefault();
                switch (result)
                {
                    case 1:
                        // Добавляем параметры качества в бд
                        try
                        {
                            foreach (var qualityParam in model.QualityParams)
                            {
                                db.AddQualityParamCompany(Convert.ToInt32(model.Id), qualityParam.Id, qualityParam.Value);
                            }
                        }
                        catch (Exception e) { };
                        return RedirectToAction("Index", "Company");
                    case -2:
                        ModelState.AddModelError("", Resources.MainResource.Company_Create_Error2);//Організація з таким іменем існує
                        break;
                    default:
                        ModelState.AddModelError("", Resources.MainResource.Company_Create_Error1);//Повідомлення про помилку
                        break;
                }
            }
            try
            {
                Directory.Delete(Server.MapPath(folder), true);
            }
            catch { };
            return View(model);
        }

        // ************************************************************************Деталі організації GET
        // GET: Company/Details/id
        [Authorize]
        public ActionResult Details(int? id)
        {
            APP_COMPANY Company = db.APP_COMPANY.Find(id);

            if (id == null)
            {
                return HttpNotFound();
            }

            if (Company != null)
            {
                CompanyDetailsViewModel model;
                List<QualityParams> list = new List<QualityParams>();
                foreach (var param in Company.APP_COMPANY_QUALITY_PARAMS)
                {
                    list.Add(new QualityParams() { Param = param.DICT_QUALITY_PARAMS.name, Value = param.value });
                }
                model = new CompanyDetailsViewModel
                {
                    Id = Company.company_id,
                    Name = Company.name,
                    Adress = Company.adress,
                    Number = Company.number,
                    ContactPerson = Company.contact_person,
                    Email = Company.email,
                    QualityParam = list,
                    NeedValidation = Company.need_validation ? Resources.MainResource.Company_Index_Yes : Resources.MainResource.Company_Index_No

                };
                if (Company.link_to_file_agreement != null)
                {
                    model.LinkToFileAgreement = Resources.MainResource.Company_Details_FileExist;
                }
                else
                {
                    model.LinkToFileAgreement = Resources.MainResource.Company_Details_FileNotExist;
                }

                return View(model);
            }
            else { return HttpNotFound(); }

        }

        public ActionResult DownloadAgreement(int? id, string name) //метод загружает договор в зависимости от формата файла
        {
            APP_COMPANY Company = db.APP_COMPANY.Find(id);
            string downloadFile = Company.link_to_file_agreement;
            name = Path.GetFileName(downloadFile);
            if (id == null)
            {
                return HttpNotFound();
            }

            if (Path.GetExtension(downloadFile) == ".docx")
            {
                return File(Server.MapPath(@"" + downloadFile), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", name);
            }
            else if (Path.GetExtension(downloadFile) == ".doc")
            {
                return File(Server.MapPath(@"" + downloadFile), "application/msword", name);
            }
            else if (Path.GetExtension(downloadFile) == ".pdf")
            {
                return File(Server.MapPath(@"" + downloadFile), "application/pdf", name);
            }
            else if (Path.GetExtension(downloadFile) == ".png")
            {
                return File(Server.MapPath(@"" + downloadFile), "image/png", name);
            }
            else if (Path.GetExtension(downloadFile) == ".jpg" || Path.GetExtension(downloadFile) == ".jpeg")
            {
                return File(Server.MapPath(@"" + downloadFile), "image/jpeg", name);
            }
            else if (Path.GetExtension(downloadFile) == ".tiff" || Path.GetExtension(downloadFile) == ".tif")
            {
                return File(Server.MapPath(@"" + downloadFile), "image/tiff", name);
            }
            else if (Path.GetExtension(downloadFile) == ".gif")
            {
                return File(Server.MapPath(@"" + downloadFile), "image/gif", name);
            }
            else if (Path.GetExtension(downloadFile) == ".zip")
            {
                return File(Server.MapPath(@"" + downloadFile), "application/zip", name);
            }
            else if (Path.GetExtension(downloadFile) == ".rar")
            {
                return File(Server.MapPath(@"" + downloadFile), "application/x-rar-compressed", name);
            }
            else return HttpNotFound();
        }

        // ************************************************************************Видалення організації POST
        [HttpPost]
        [ActionName("Delete")]
        public string DeleteConfirmed(int? id)
        {
            APP_COMPANY company = db.APP_COMPANY.Find(id);
            var result = db.DeleteCompany(id).FirstOrDefault();
            if (result == 0)
            {
                string path = company.link_to_file_agreement;
                if (path != null) //если есть ссылка на файл, удаляет 
                {
                    try
                    {
                        Directory.Delete(Server.MapPath(path.Replace(Path.GetFileName(path), "")), true);
                    }
                    catch { }
                }
                return "DeleteSuccess"; //Видалення успішне (0)
            }
            else if (result == -1)
            {
                return "DeleteError_UsersInSystem"; //Видалення неможливе, в системі присутні користувачі цієї організації (-1)
            }
            else if (result == -2)
                return "DeleteError_NoCompany"; // Видалення неможливе, в системі відсутня задана організація (-2)
            else
            {
                return "DeleteError_ServiceObjectsInSystem"; // Видалення неможливе, для даної організації зареєстровані об’єкти обслуговування (-3) */
            }
        }

        // ************************************************************************Отримання об’єктів обслуговування за id організації
        [HttpPost]
        public string GetServiceObjectsByCompanyID(int? id)
        {
            IList<string> ServiceObjects = db.GetServiceObjectsByCompanyID(id).ToList();
            return string.Join(", ", ServiceObjects);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
