﻿using Compass.EntityModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compass.Controllers
{
    public class EquipmentController : Controller
    {
        CompassEntities db = new CompassEntities();

        // GET: Equipment
        [Authorize]
        public ActionResult Index(int? id)
        {

            if (id == null)
                return View(db.APP_Equipment);
            else
                return View(db.APP_Equipment.Where(item => item.APP_ServiceObjects.CompanyId == id));
        }

        [Authorize]
        public ActionResult ManageCategories()
        {
            return View(db.DICT_EquipmentCategories);
        }

        [HttpPost]
        public int CreateCategory(string name)
        {
            //if (name == null)
            //{
            //    return -1;
            //}

            DICT_EquipmentCategories category = new DICT_EquipmentCategories { Name = name };
            db.DICT_EquipmentCategories.Add(category);
            db.SaveChanges();

            return category.Id;
        }

        [HttpPost]
        public int EditCategory(DICT_EquipmentCategories category)
        {
            db.Entry(category).State = EntityState.Modified;
            db.SaveChanges();
            return 1;
        }

        [HttpPost]
        public int DeleteCategory(DICT_EquipmentCategories category)
        {
            db.Entry(category).State = EntityState.Deleted;
            db.SaveChanges();
            return 1;
        }

        [HttpPost]
        [ActionName("Delete")]
        public string DeleteConfirmed(int id)
        {
            int result = db.DeleteEquipment(id);
            if (result == -1)
            {
                return "DeleteError";
            }
            else return "DeleteSuccess";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
